package _3_apisnucleojava._5_clasearraylist;

import java.util.ArrayList;
import java.util.List;

public class Leccion3_Autoboxing {

    public static void main(String... arg) {
        List<Integer> listIntegers = new ArrayList<>();
        listIntegers.add(new Integer(1));
        listIntegers.add(new Integer(2));
        listIntegers.add(new Integer(3));
        listIntegers.add(null);
        System.out.println(listIntegers);

        /*
        * Quiere esto decir que cada vez que queramos añadir un elemento a la
        * lista tenemos que crear un objeto Integer?
        * 
        * No, afortunadamente desde Java 5 podemos utilizar un mecanismo
        * llamado Autoboxing que básicamente consiste en que es el propio Java
        * quien hace las conversiones apropiadas tanto en un sentido (de tipo
        * primitivo a objeto inBoxing) como en otro (de objeto a tipo primitivo
        * outBoxing).
         */
        listIntegers.add(4); //por autoboxing se conviert de int primitivo a Integer
        System.out.println(listIntegers);

        int k = listIntegers.get(0); //devuelve el integer de la primera posicion del array list
        System.out.println("k: " + k);

        // int intNull = list7.get(3); //java.lang.NullPointerException
        Integer integerNull = listIntegers.get(3);
        System.out.println("integerNull: " + integerNull);

        List<Integer> numbers = new ArrayList<>();
        numbers.add(1);
        numbers.add(2);
        System.out.println("numbers: " + numbers);
        numbers.remove(1); //indice debe ser primitivo
        System.out.println("numbers remove indice 1: " + numbers);
        numbers.remove(new Integer(1));
        System.out.println("numbers remove el objeteo new Integer(1): " + numbers);


        /*
        * El método remove elimina un objeto o el objeto en una posición
        * concreta, por lo que puede haber cierta confusión.
        * 
        * Recuerda, en este caso remove elimina el objeto en la posición 1 (es
        * decir el 2) y no el Integer con valor 1.
        * 
        * Si quieres eliminar el Integer con valor 1, tienes que hacerlo
        * explícitamente:
        * 
        * numbers.remove(new Integer(1));
         */
    }

}
