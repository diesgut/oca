package _3_apisnucleojava._5_clasearraylist;

public class Leccion2_Wrapper_Classes {

    /**
     * Hasta el momento solo hemos colocado objectos string, en los arraylist,
     * pero que pasa si queremos agregar tipo de datos primitivos. Para estos
     * casos existen las Wrapper Classes o clases envolventes, cada tipo de dato
     * primitivo tiene su wrapper class.
     */
    /*
        * boolean -> Boolean. new Boolean(true)
        * byte -> Byte. new Byte((byte) 1)
        * short -> Short. new Short((short) 1)
        * int -> Integer. new Integer(1)
        * long -> Long. new Long(1)
        * float -> Float. new Float(1.0)
        * double -> Double. new Double(1.0)
        * char -> Character. new Character('a')
     */
    /**
     * las wrapper classes, tienen metodos para volver a convertir nuevamente a
     * tipos primitivos (intValue(), etc.) (aunque el autoboxing permite
     * prescindir de estos).
     */
    //Nota: el metodo parseInt() devuelve primitios, y el metodo valueOf() devuelve wrapper class.
    int primitive = Integer.parseInt("123");
    Integer wrapper = Integer.valueOf("123");

    /*
        * Además, cada clase envolvente proporciona un método para pasar de String a su tipo primitivo correspondiente:
        * Boolean.parseBoolean("true");
        * Byte.parseByte("1");
        * Short.parseShort("1");
        * Integer.parseInt("1");
        * Long.parseLong("1");
        * Float.parseFloat("1");
        * Double.parseDouble("1");
        * 
        * La família de métodos valueOf se utilizan para pasar de String a su
        * clase envolvente correspondiente:
        * 
        * Boolean.valueOf("TRUE");
        * Byte.valueOf("2");
        * Short.valueOf("2");
        * Integer.valueOf("2");
        * Long.valueOf("2");
        * Float.valueOf("2.2");
        * Double.valueOf("2.2");
     */
    
}
