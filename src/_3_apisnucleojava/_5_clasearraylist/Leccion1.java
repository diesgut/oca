package _3_apisnucleojava._5_clasearraylist;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * La clase ArrayList
 *
 * Al crear un array debemos saber cuantos elementos tendrá, pero este no es
 * siempre el caso. El tamaño de la clase ArrayList igual que la clase
 * StringBuilder puede cambiar en tiempo de ejecución.
 *
 * Recuerda que si durante el examen te muestran un fragmento de código que no
 * empieza por 1, puedes asumir que todos los imports requeridos están
 * presentes. De la misma manera, también puedes asumir que todos los imports
 * están presentes si te muestran un método o un fragmento de un método.
 * java.util.*; or java.util.Array; or java.util.ArrayList
 */
public class Leccion1 {

    public static void main(String[] args) {
        System.out.println("1. Creando ArrayList");
        /*
        * A la hora de crear un objeto ArrayList tenemos tres constructores:
        * 
        * 1. El constructo por defecto.
        * 2. Con una capacidad específica.
        * 3. Haciendo una copia de otro ArrayList.
         */
        ArrayList objectList = new ArrayList(); //lista de objetos, cualquier ojeto menos primitivos
        ArrayList objectList10 = new ArrayList(10);
        ArrayList list3 = new ArrayList(objectList10);

        // Desde Java 5, con la introducción de genéricos, podemos especificar el tipo de contenido.
        ArrayList<String> stringList = new ArrayList<String>();

        // Y desde Java 7, podemos omitir el tipo a la hora de invocar al constructor, puesto que el compilador lo puede inferir. 
        // Esto se consigue con el operador diamante (diamond)
        ArrayList<String> list5 = new ArrayList<>();

        // Puesto que la clase ArrayList implementa la interfaz List, esto es totalmente válido.
        List<String> list6 = new ArrayList<>();

        //  ArrayList<String> list7 = new List<>(); no compila
        System.out.println("2. Usando ArrayList");

        System.out.println("2.1. Add");
        {
            /*
        * El método add es probablemente el método más utilizado. Como no hemos especificado ningún parámetro en el momento de la creación, Java
        * asume Object, es decir, cualquier objeto.
             */
            objectList.add("Alfa");
            objectList.add(123);
            objectList.add(Boolean.TRUE);
            System.out.println("list1: " + objectList);

            /*
        * Pero qué ocurre con list4? Dado que hemos especificado que contenga
        * sólo cadenas el compilador detectara cualquier intento de uso
        * indebido.
             */
            List<String> birds = new ArrayList<String>();
            birds.add("hawk"); //[hawk]
            birds.add(1, "robin");//[hawk,robin]
            birds.add(0, "blue jay");// [blue jay, hawk, robin]
            birds.add(1, "cardinal"); //[blue jay,cardinal, hawk, robin]
            System.out.println(birds); //[blue jay,cardinal, hawk, robin]

            // El método remove, elimina el primer elemento que coincida con el argumento, o un elemento en una posición específica.
            stringList.remove("Bravo");
            System.out.println("list4: " + stringList);
            //  stringList.remove(0);
            System.out.println("list4: " + stringList);

        }
        System.out.println("2.2 Remove");
        //remueve el primer valor coincident encontrado en un arraylist o el elemento de un indice especifico
        {
            List<String> birds = new ArrayList<String>();
            birds.add("hawk"); //[hawk]
            birds.add("hawk"); //[hawk,hawk]
            System.out.println(birds.remove("cardinal")); //print false
            System.out.println(birds.remove("hawk")); //print true

            System.out.println(birds.remove(0)); //print hawk, cuando removemos usando el indice, devuelve el objeto que se esta removiendo
            System.out.println(birds); //print []
            // Si intentamos eliminar un índice que no existe, obtenemos una excepción java.lang.IndexOutOfBoundsException.

            birds.add("cardinal");
            // También existe el método removeIf, pero lo trataremos más tarde porque utiliza expresiones lambda.
            birds.removeIf(e -> e.startsWith("c"));
            System.out.println(birds);
        }
        System.out.println("2.3 set");
        {
            //cambiauno de los elementos del arraylist. sin modificar su tamaño
            List<String> birds = new ArrayList<String>();
            birds.add("hawk"); //[hawk]
            System.out.println(birds.size());//1
            System.out.println(birds.set(0, "robin")); //devuelve el objeto a ser removido
            System.out.println(birds.size());
            //birds.set(1, "robin");//java.lang.IndexOutOfBoundsException

        }
        System.out.println("2.4 isEmpty() y size()");
        {
            List<String> birds = new ArrayList<String>();
            System.out.println(birds.isEmpty());//true
            birds.add("hawk"); //[hawk]
            birds.add("hawk"); //[hawk, hawk]
            System.out.println(birds.isEmpty());//false
            System.out.println(birds.size());//2
        }
        System.out.println("2.5 clear()");
        {
            //descarta todos los elementos en un arraylist sin perder la referencia
            List<String> birds = new ArrayList<>();
            birds.add("hawk"); //[hawk]
            birds.add("hawk"); //[hawk, hawk]
            System.out.println(birds.isEmpty());//false
            System.out.println(birds.size());//2
            birds.clear();
            System.out.println(birds.isEmpty());//true
            System.out.println(birds.size());//0
        }
        System.out.println("2.6. contains()");
        {
            //comprueba si un valor se encuentra en el arraylist (usa el metodo equals)
            List<String> birds = new ArrayList<>();
            birds.add("hawk"); //[hawk]
            System.out.println(birds.contains("hawk")); //true
            System.out.println(birds.contains("robin")); //false
        }
        System.out.println("2.7. equals()");
        {
            /**
             * arraylist tiene una implementacion especial del metodo equals que
             * compara si dos listas tienen los mismos elementos en el mismo
             * orden
             */
            List<String> one = new ArrayList<>();
            List<String> two = new ArrayList<>();
            System.out.println(one.equals(two));//true
            one.add("a"); //[a]
            System.out.println(one.equals(two));//false
            two.add("a"); //[a] 
            System.out.println(one.equals(two));//true

        }
        System.out.println("3. Convertir entre array y list");
        {
            System.out.println("3.1 List a Array");
            List<String> list = new ArrayList<>();
            list.add("hawk");
            list.add("robin");
            System.out.println("list: " + list);
            Object[] objectArray = list.toArray();
            System.out.println("objectArray: " + Arrays.toString(objectArray));//2
            String[] stringArray = list.toArray(new String[0]);
            System.out.println("stringArray: " + Arrays.toString(stringArray)); //2
            /*
        * Convertir entre arrays y listas y viceversa es una tarea común. El
        * método toArray hace justamente eso, pero por defecto convierte a un
        * array de objetos que seguramente no es lo que queremos en la mayoría
        * de los casos.
        * 
        * El método toArray acepta un parámetro de tipo array. La ventaja de
        * especificar un cero como tamaño del array que pasamos como argumento
        * al parámetro del metodo toArray es que Java determine el tamaño
        * adecuado por sí mismo.
        * 
        * Aunque nosostros podemos sugerir un tamaño, que si es mayor que el
        * número de elementos, Java rellenará con valores null y si es menor,
        * Java sencillamente lo ignorará y creará uno nuevo que se ajuste al
        * tamaño real.
             */
            System.out.println("3.1 Array a List");

            String[] arrayBirds = {"hawk", "robin"};
            System.out.println("arrayBirds: " + Arrays.toString(arrayBirds));
            List<String> listBirds = Arrays.asList(arrayBirds); //lista de tamaño fijo, y enlazada al arreglo arrayBirds

            System.out.println("listBirds: " + listBirds);

            listBirds.set(1, "test");
            System.out.println("listBirds.set(1, \"test\"): " + listBirds);
            arrayBirds[0] = "new";
            System.out.println("arrayBirds[0] = \"new\": " + Arrays.toString(arrayBirds));
            System.out.println("listBirds: " + listBirds);
            //listBirds.remove(1); //throwns UnsupportedOperation Exception

            /*
        * Convertir de una array a un lista es más interesante.
        * 
        * El array original y la lista estan enlazados: cuando se hace un
        * cambio en uno de los dos el cambio está automáticamente presente en
        * el otro.
        * 
        * La lista de respaldo (backed List) como se la conoce es una
        * implementación particular de la interfaz List, de tamaño fijo, que
        * lanza una excepción java.lang.UnsupportedOperationException si
        * intentamos añadir elementos.
             */
        }

        System.out.println("4. Ordenamiento");
        {
            //El ordenamiento de arraylist es similar a los array, con la diferencia de una clase utilitaria

            /*
        * No forma parte del examen, pero el método asList de la clase Arrays
        * que declara un parámetro Varargs es una alternativa interesante a la
        * hora de crear un ArrayList en un sola línia.
             */
            List<String> listVarargs = Arrays.asList("Alfa", "Bravo", "Charlie", "Delta",
                    "Echo", "Foxtrot", "Golf", "Hotel", "India", "Juliett", "Kilo",
                    "Lima", "Mike", "November", "Oscar", "Papa", "Quebec", "Romeo",
                    "Sierra", "Tango", "Uniform", "Victor", "Wiskey", "Xray",
                    "Yankee", "Zulu");
            System.out.println("listVarargs: " + listVarargs);

            /*
        * Finalmente, podemos desordenar, girar y ordenar una lista fácilmente
        * con los métodos shuffle, reverse y sort respectivamente de la clase
        * Collections.
             */
            Collections.shuffle(listVarargs);
            System.out.println("Collections.shuffle(listVarargs): " + listVarargs);
            Collections.reverse(listVarargs);
            System.out.println("Collections.reverse(listVarargs): " + listVarargs);
            Collections.sort(listVarargs);
            System.out.println("Collections.sort(listVarargs): " + listVarargs);

        }

    }

}
