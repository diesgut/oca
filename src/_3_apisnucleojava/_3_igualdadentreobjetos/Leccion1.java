package _3_apisnucleojava._3_igualdadentreobjetos;

/**
 * La igualdad entre objetos
 *
 * Te ha planteado alguna vez cuándo decimos que dos objetos, por ejemplo, de
 * tipo ‘Vehiculo’ son iguales?
 *
 * Se consigue sobrescribiendo el metodo equals() de la clase Object. Aunque en
 * el examen OCA no necesitas saber cómo se implementa el método equals.
 *
 */
public class Leccion1 {
    
    public static void main(String[] args) {

        //en este ejemplo probaremos con objetos StringgBuilder, y ya que no se trata
        //de tipo primitivo el operador == comprueba si son la misma referencia
        StringBuilder one = new StringBuilder("abc");
        StringBuilder two = new StringBuilder("abc");
        StringBuilder three = one.append("d");
        System.out.println("one == two: " + (one == two)); //one == two: false
        System.out.println("one == three: " + (one == three));//one == three: true
        /*
        * Lo que sí debes saber es que la clase StringBuilder NO implementa el método equals, 
        sino que lo hereda de Object con la implementación por defecto,  por lo tanto se comporta como '=='.
         */
        System.out.println("one.equals(two): " + one.equals(two)); //false
        System.out.println("one.equals(three): " + one.equals(three)); //true

        /**
         * Recordar que los objetos de la clase string son inmutables, y las
         * cadenas creadas sin el operador new, son madandas al pool de strings.
         */
        {
            String x = "Hello World";
            String y = "Hello World";
            System.out.println(x == y);//true
        }
        {
            String x = "Hello World";
            String y = "Hello World".trim();//si no hay camios trim no crea un nuevo objeto
            String z = " Hello World".trim();
            //aunqe x y z tienen el mismo valor y son creados son el operador new
            //al usar metodos del objeto string, estos devuelven nuevas instancias de string
            System.out.println(x == y);//true, 
            System.out.println(x == z);//false,
        }
        {
            String x = "Hello World";
            String y = new String("Hello World");
            //el primer string es poleable el segundo no
            System.out.println(x == y);//false
        }
        /**
         * . El metodo equals. La clase string sobreescrive el metodo equals que
         * compara el valor de la clase string y no la referencia.
         */
        {
            /**
             * Nunca se debe usar == para comparar cadenas, la unica vez que se
             * debe usar es en el examen, si lo queremos es una igualdad logica
             * en lugar de una igualdad de objetos, en la clase string podemos
             * usar el metodo equals.
             */
            String x = "Hello World";
            String z = "  Hello World".trim();
            System.out.println(x.equals(z));//true
            
            Tiger t1 = new Tiger();
            Tiger t2 = new Tiger();
            Tiger t3 = t1;
            
            System.out.println(t1 == t3);//true
            System.out.println(t1 == t2);//false
            System.out.println(t1.equals(t2));//false no implementa equals
            System.out.println(t1.equals(t3)); //true, no implementa equals y equals se comporta igual que ==
            
        }
    }
    
}

class Tiger {
    
    String name;
    
}
