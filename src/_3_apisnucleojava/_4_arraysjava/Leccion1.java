package _3_apisnucleojava._4_arraysjava;

import java.util.Arrays;

/**
 *
 * Los arrays de Java
 *
 * Tanto la clase String como la clase StringBuilder están implementadas con una
 * array de caracteres.
 *
 * La clase StringBuilder sustituye su array de caracteres por otro cuando se
 * queda sin espacio.
 *
 */
public class Leccion1 {

    public static void main(String[] args) {

        System.out.println("1. Creacion Arrays de primitivos");
        {
            //Creando un arreglo de esta forma, se crea un arreglo con 3 elementos y estos tienen el valor por defecto 
            //segun su tipo
            int[] numbers1 = new int[3];
            int[] arg1, arg2; //dos variables arrays de int
            //   int numbers1[] = new int[3]; correct
            //   int[] numbers1 = new int[3]; correct

            for (int i = 0; i < numbers1.length; i++) {
                System.out.println(numbers1[i]); //0
            }

            // Esta es otra forma de crear un array especificando cuáles son sus elementos.
            int[] numbers2 = new int[]{42, 55, 99};
            for (int i = 0; i < numbers2.length; i++) {
                System.out.println(numbers2[i]);
            }

            // Esta es una versión reducida de la anterior, más conveniente. En este caso creamos un array anónimo.
            int[] numbers3 = {42, 55, 99};
            for (int i = 0; i < numbers3.length; i++) {
                System.out.println(numbers3[i]);
            }

            // En ocasiones puedes encontrar los corchetes junto a la variable, en principio, es equivalente.
            int numbers4[] = {42, 55, 99};

            // Pero atención en este ejemplo, a es un array de enteros y b un entero. Mientras que c y d, ambos son arrays de enteros.
            int a[], b;
            int[] c, d;
        }
        System.out.println("2. Creacion Arrays con Variables de Referencia");
        {
            /*
		 * Los arrays pueden ser de cualquier tipo, incluso de clases creadas por nosotros.
		 * 
		 * Un array de cadenas por ejemplo, no contiene cadenas sino referencias a cadenas y lo mismo pasa con cualquier otro tipo.
		 * 
		 * La implementación del método equals de un array simplemente comprueba si apuntan a la misma referencia (asi sean primitivos).
             */
            String[] cadenas1 = {"alfa", "bravo", "charlie", "delta", "echo"};
            // String[] cadenas2 = { "alfa", "bravo", "charlie", "delta", "echo" };
            String[] cadenas2 = cadenas1;

            System.out.println("cadenas1: " + Arrays.toString(cadenas1));
            System.out.println("cadenas1.equals(cadenas2): " + cadenas1.equals(cadenas2));
            System.out.println("cadenas1 == cadenas2: " + (cadenas1 == cadenas2));
            System.out.println("cadenas1: " + cadenas1);
            System.out.println("cadenas2: " + cadenas2);

            //Casting, forzando un tipo mayor a uno menor
            String[] strings = {"stringValue"};
            Object[] objects = strings;
            String[] againStrings = (String[]) objects;
            //againStrings[0]=new StringBuilder(); no compila
            //  objects[0] = new StringBuilder(); //cuidado, error tiempo ejecucion java.lang.ArrayStoreException
            /**
             * para este ejemplo primero creamos una arreglo de strings, podemos
             * guardarlo dentro de un array de objects por que objects es mas
             * amplio que string, luego el array de objects podemos guardarlo en
             * un variable tipo array string previo casting. pero al querer
             * guardar un stringbuilder en el arreglo de objetos nos arrojara
             * una exepcion java.lang.ArrayStoreException
             */
            Object[] objects2 = new String[]{"alfa", "bravo", "charlie", "delta"};
            System.out.println(Arrays.toString(objects2));
            // Lanza java.lang.ArrayStoreException en tiempo de ejecución.

            /////////////////
            // Considera este ejemplo donde el array de cadenas cadenas3 tiene tres elementos: alfa, bravo y charlie.
            // Fijate cómo a pesar de asignar null a alfa, bravo y charlie, el array sigue manteniento sus referencias intactas.
            String alfa = "alfa", bravo = "bravo", charlie = "charlie";
            String[] cadenas3 = {alfa, bravo, charlie};
            System.out.println("Arrays.toString(cadenas3): " + Arrays.toString(cadenas3));
            alfa = bravo = charlie = null;

            // cadenas3 continúa guardando las referencias iniciales.
            System.out.println("Arrays.toString(cadenas3): " + Arrays.toString(cadenas3));
        }
        System.out.println("4. Sorting");
        {
            //Require java.util* or java.util.Array
            String[] wordsArg = {"cana", "caña", "cama"};
            char[] letterArg = {'c', 'b', 'a'};
            int[] integerArg = {3, 2, 1};
            String[] stringIntArg = {"10", "9", "100"};
            System.out.println("Original Arrays.toString(wordsArg): " + Arrays.toString(wordsArg));
            System.out.println("Original Arrays.toString(letterArg): " + Arrays.toString(letterArg));
            System.out.println("Original Arrays.toString(integerArg): " + Arrays.toString(integerArg));
            System.out.println("Original Arrays.toString(stringIntArg): " + Arrays.toString(stringIntArg));
            Arrays.sort(wordsArg);
            Arrays.sort(letterArg);
            Arrays.sort(integerArg);
            System.out.println("Sorting Arrays.toString(wordsArg): " + Arrays.toString(wordsArg));
            System.out.println("Sorting Arrays.toString(letterArg): " + Arrays.toString(letterArg));
            System.out.println("Sorting Arrays.toString(integerArg): " + Arrays.toString(integerArg));
            System.out.println("Original Arrays.toString(stringIntArg): " + Arrays.toString(stringIntArg));
            //El ultimo caso no se ordena como numeros, puesto que string ordena de forma alfabetica int 1 antes que 9,
            //(el orden de string es números ordenados antes de letras y mayúsculas antes de minúsculas, en caso de que te lo preguntes)
        }
        System.out.println("5. Searching");
        {
            /**
             * El método binarySearch busca elementos dentro de una array
             * ordenado, por ejemplo con el método sort() de la clase Arrays. Si
             * un array no está ordenado, el resultado de la búsqueda será
             * impredecible.
             */
            String[] stringArray = {"sergio", "claudia", "javier", "andrea", "daniel"};
            System.out.println("Arrays.toString(stringArray) " + Arrays.toString(stringArray));
            Arrays.sort(stringArray);
            // 
            int pos = Arrays.binarySearch(stringArray, "andrea");
            System.out.println("stringArray[pos]: Andrea " + pos + ", value=" + stringArray[pos]);
            pos = Arrays.binarySearch(stringArray, "sergio");
            System.out.println("stringArray[pos]: sergio " + pos + ", value=" + stringArray[pos]);
            pos = Arrays.binarySearch(stringArray, "diego");
            System.out.println("stringArray[pos]: diego " + pos + ", value=");
        }
        System.out.println("6. Multidimensional Array");
        {
            //Los arrays d java son objetos que pueden contener otros objetos por lo tanto puede contener arrays
            /**
             * Java interpreta los arrays multidimensionales como arrays de
             * arrays. Las matrices o arrays bidimensionales pueden ser
             * simétricas de m x n elementos, o asimétricas donde cada fila
             * puede tener un número diferente de columnas.
             */
            int[][] vars1; //2d array
            int var2[][];// 2d array
            int[] var3[];//2d array
            int[] var4[], space[][];//2d and 3d array

            String[][] rectangule = new String[3][2];
            //rectangule es un arreglo de 3 elementos y cada elemento es un arreglo de dos elementos
            rectangule[0][1] = "set";//accedemos al primer elemento que es un arreglo y seteamos a su primer elemento la palabra set

            int[][] differentSize = {{1, 4}, {3}, {9, 8, 7}};
            //se crea un arreglo de 3 elementos donde cada elemento es un arreglo, 
            //el primer sub array es de 2 elementos, el segundo subarray es de un elemento y el tercer subarray es de 3 elementos

            System.out.println("6.1 Usando Multidemensional array");
            int[][] matriz1 = new int[3][4];
            int[][] matriz2 = new int[3][];
            matriz2[0] = new int[3];
            matriz2[1] = new int[4];
            matriz2[2] = new int[2];

            for (int i = 0; i < matriz1.length; i++) {
                for (int j = 0; j < matriz1[i].length; j++) {
                    System.out.print(matriz1[i][j] + " ");
                }
                System.out.println();
            }
            System.out.println("********");
            for (int i = 0; i < matriz2.length; i++) {
                for (int j = 0; j < matriz2[i].length; j++) {
                    System.out.print(matriz2[i][j] + " ");
                }
                System.out.println();
            }

            // Alternativamente puede recorrerse con dos bucles for-Each.
            System.out.println("********");
            for (int[] i : matriz2) {
                for (int j : i) {
                    System.out.print(j + " ");
                }
                System.out.println();
            }
        }

    }

}
