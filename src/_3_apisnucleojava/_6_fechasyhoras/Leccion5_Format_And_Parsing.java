package _3_apisnucleojava._6_fechasyhoras;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

public class Leccion5_Format_And_Parsing {

    public static void main(String[] args) {
        //Date and Time tienen metodos para traer informacion de ellos
        {
            LocalDate date = LocalDate.of(2020, Month.JANUARY, 20);
            System.out.println(date.getDayOfWeek()); //Monday
            System.out.println(date.getMonth()); //January
            System.out.println(date.getYear()); //2020
            System.out.println(date.getDayOfYear());//20

        }
        System.out.println("\n1. Formating \n");
        {
            /**
             * Java ofrece provee la clase DateTimeFormatter para ayudarnos
             * cambiar el formato de las fechas y horas, esta clase se encuentra
             * en el paquete java.time.format. (De fechas a string)
             */

            LocalDate date = LocalDate.of(2020, Month.JANUARY, 20);
            LocalTime time = LocalTime.of(11, 12, 34);
            LocalDateTime dateTime = LocalDateTime.of(date, time);
            System.out.println("format date: " + date.format(DateTimeFormatter.ISO_LOCAL_DATE)); //2020-01-20
            System.out.println("format time: " + time.format(DateTimeFormatter.ISO_LOCAL_TIME)); //11:12:34
            System.out.println("format datetime: " + dateTime.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME)); //2020-01-20T11:12:34
            /**
             * ISO es una estándar para las fechas.
             */

            /**
             * La clase DateTimeFormatter de Java 8 viene a ser como la clase
             * SimpleDateFormat de Java 7. la clase DateTimeFormatter provee
             * formatos predeterminados
             */
            DateTimeFormatter shortDateTime = DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT);

            System.out.println("dateTime short date time format 1: " + shortDateTime.format(dateTime));   // 1/20/20
            System.out.println("date short date time format 1: " + shortDateTime.format(date));  // 1/20/20
            // System.out.println(shortDateTime.format(time)); // UnsupportedTemporalTypeException
            /**
             * Este código es equivalente al anterior. Se puede utilizar uno u
             * otro indistintamente.
             */
            System.out.println("dateTime short date time format 2: " + dateTime.format(shortDateTime));
            System.out.println("date short date time format 2: " + date.format(shortDateTime));
            // System.out.println(time.format(shortDateTime));  // UnsupportedTemporalTypeException

            /**
             * Los dos formatos predefinidos que veremos en el examen son SHORT
             * y MEDIUM, los otras formatos que no apareceran incluyen tiempo.
             */
            {
                LocalDate fecha = LocalDate.of(2020, Month.JANUARY, 20);
                LocalTime hora = LocalTime.of(11, 12, 34);
                LocalDateTime fechaHora = LocalDateTime.of(fecha, hora);

                DateTimeFormatter shortF = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT);
                DateTimeFormatter mediumF = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM);
                System.out.println("shortF " + shortF.format(fechaHora)); //20/01/20 11:12 AM
                System.out.println("mediumF " + mediumF.format(fechaHora)); //20/01/2020 11:12:34 AM
            }
            /*
        * Si por algún motivo no queremos utilizar los formatos predeterminados (SHORT, MEDIUM, LONG o FULL), podemos crear nuestros propios formatos
        * con los siguientes códigos:
        * 
        * MMMM representa el mes del año: M muestra el mes como un dígito. MM muestra el mes como dos dígitos. MMM muestra el mes como por ejemplo
        * 'Ene' y MMMM muestra el mes como por ejemplo 'Enero'.
        * 
        * dd representa el dia del mes. Como en el caso del mes el numero de d's determina el formato del dia.
        * 
        * yyyy representa el año.
        * 
        * mm representa los minutos.
        * : Usar si quieres mostrar dos puntos
        * 
        * hh representa la hora.
             */
            DateTimeFormatter dateTimeFormatter1 = DateTimeFormatter.ofPattern("MMMM dd, yyyy, hh:mm");
            System.out.println("dateTime dateTimeFormatter1: " + dateTime.format(dateTimeFormatter1));

        }
        System.out.println("\n2. Parsing \n");
        {
            /*
        * Por último, nos queda por tratar el tema de la conversión de una
        * cadena en un objeto fecha.
        * 
        * Tanto la clase LocalDate, LocalTime como LocalDateTime proporcionan
        * un método estático, parse, que puede transformar una cadena en fecha.
             */
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM dd yyyy");
            LocalDate date = LocalDate.parse("01 02 2015", formatter);
            LocalTime time = LocalTime.parse("11:22");
            
            System.out.println("localDate: " + date); // 2015-01-02
            System.out.println("localTime: " + time);  // 11:22
        }
    }
}
