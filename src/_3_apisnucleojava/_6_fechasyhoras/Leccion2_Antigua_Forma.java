package _3_apisnucleojava._6_fechasyhoras;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class Leccion2_Antigua_Forma {

    public static void main(String[] args) {

        /*
        * En versiones anteriores a Java 8, usábamos las clases Date y
        * Calendar.
        * 
        * Actualmente, la mayoria de constructores de la clase Date no están recomendados (deprecated), 
        y los meses empiezan en 0 lo que añade confusión.
         */
        Date date1 = new Date();
        System.out.println("date1: " + date1);

        Calendar calendar1 = Calendar.getInstance();
        calendar1.set(2016, 4, 21);
        System.out.println("calendar1: " + calendar1);

        Date date2 = calendar1.getTime();
        System.out.println("date2: " + date2);

        Calendar calendar2 = new GregorianCalendar(2016, Calendar.APRIL, 21);
        Date date3 = calendar2.getTime();
        Date date4 = new Date(2015, Calendar.JANUARY, 12); // Deprecated

    }
}
