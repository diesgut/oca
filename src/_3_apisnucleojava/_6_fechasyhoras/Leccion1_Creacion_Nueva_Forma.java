package _3_apisnucleojava._6_fechasyhoras;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

/**
 *
 * Trabajando con fechas y horas
 *
 * Java 8 ha renovado completamente el trabajo con fechas, horas, períodos, y
 * zonas horarias: con las clases LocalDate, LocalTime, LocalDateTime entre
 * otras.
 *
 * En el examen suele quedar claro el formato de las fechas (Unos paises
 * utilizan el formato 'dd/MM/yyyy' como España, otros el formato 'yyyy-MM-dd'
 * como los Estados Unidos de Norte América), además Java usa un formato de 24
 * horas.
 *
 * la forma renovada del maneja de fecha hace uso de las clases de java.time.*
 *
 */
public class Leccion1_Creacion_Nueva_Forma {

    public static void main(String[] args) {
        /*
        for (int i = 0; i < 10;) {
            i = i++;
            System.out.println("xxx" + i);
        }
         */

        System.out.println("1. Creacion de Fechas y Horas \n");
        {
            /*
        * La clase java.time.LocalDate contiene sólo una fecha, con formato
        * ISO-8601 como 2010-08-31, sin hora y sin zona horaria
        
        * La clase java.time.LocalTime contiene sólo hora (hora, minutos, nanosegundos, 24 horas), sin fecha y sin zona horaria
        * La clase java.time.LocalDateTime contiene fecha y hora pero sin zona horaria.
        * La clase java.time.ZonedDateTime contie fecha, hora y zona horario, aunqe oracle recomienda no hacer uso de la zona horaria.
        * 
        * No se usan los constructores para crear nuevas instancias sino los
        * métodos estáticos of() y now().
             */
            System.out.println("LocalDate.now(): " + LocalDate.now()); //2018-12-07
            System.out.println("LocalTime.now(): " + LocalTime.now()); //22:20:53.128
            System.out.println("LocalDateTime.now(): " + LocalDateTime.now()); //2018-12-07T22:20:53.128

            //Creando fechas especificas
            /*
        * Estas dos fechas son equivalentes. Si bien el tipo enumerado Month no
        * forma parte del examen OCA.
            * El mes empieza desde 1 para new date y metodos de horas
             */
            LocalDate fecha1 = LocalDate.of(2015, 1, 20);
            LocalDate fecha3 = LocalDate.of(2015, Month.JANUARY, 20);

            /*
        * Las horas pueden tener diferentes niveles de detalle:
        * 
        * 1. Hora y minutos.
        * 2. Hora, minutos y segundos.
        * 3. Hora, minutos, segundos y nanosegundos.
             */
            LocalTime hora1 = LocalTime.of(6, 15);
            LocalTime hora2 = LocalTime.of(6, 15, 30);
            LocalTime hora3 = LocalTime.of(6, 15, 30, 200);

            // También se puede combinar la fecha y la hora en un mismo objeto.
            // SIEMPRE a través de métodos estáticos.
            LocalDateTime fechaHora1 = LocalDateTime.of(2015, Month.JANUARY, 20, 6,
                    15, 30);
            LocalDateTime fechaHora2 = LocalDateTime.of(fecha1, hora1);

            /* Posibls trampas */
            //  LocalDate d=new LocalDate(); error compilacion
            //LocalDate.of(2015, Month.JANUARY, 32); throws DateTimeException
            //valores validos para day of month 1-28/31
        }

    }
}
