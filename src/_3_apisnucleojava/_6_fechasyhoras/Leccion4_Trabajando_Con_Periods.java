package _3_apisnucleojava._6_fechasyhoras;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.Period;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;

public class Leccion4_Trabajando_Con_Periods {

    public static void main(String[] args) {

        /**
         * Nota: localdate y localdatetime tienen un método para convertir en
         * equivalentes long en relación a 1970. ¿Qué tiene de especial 1970?
         * eso es lo que UNIX comenzó a usar para los estándares de datos, por
         * lo que Java lo reutilizó. y no se preocupe, no tiene que memorizar
         * los nombres para el examen.
         */
        LocalDate localDate = LocalDate.now();
        System.out.println("localDate.toEpochDay(): " + localDate.toEpochDay()); //numero de dias desde january 1 1970
        LocalDateTime localDateTime = LocalDateTime.now();
        System.out.println("localDateTime.toEpochSecond(ZoneOffset.UTC): " + localDateTime.toEpochSecond(ZoneOffset.UTC)); //numero de segundos desde january 1 1970

        System.out.println("\n1. Periods \n");
        {
            // En Java 8 Java define la clase Period. Hay cinco maneras de definir un período
            // Estos son algunos ejemplos de periodos (años, meses y días): Cada año
            Period anual = Period.ofYears(1); //cada 2 años
            System.out.println("anual: " + anual);
            // Cada trimestre
            Period trimestral = Period.ofMonths(3); //cada 3 meses
            System.out.println("trimestral: " + trimestral);
            // Cada tres semanas
            Period everThreeWeeks = Period.ofWeeks(3); //cada 3 seamanas
            System.out.println("everThreeWeeks: " + everThreeWeeks);
            // Día sí y dia no, cada dos días
            Period everyTwoDays = Period.ofDays(2); //cada 2 dias
            System.out.println("everyTwoDays: " + everyTwoDays);
            // Cada año y siete días
            Period everyYearAndWeek = Period.of(1, 0, 7); //cada año y 7 dias
            System.out.println("everyYearAndWeek: " + everyYearAndWeek);
            /*
        * Los períodos no se pueden encadenar al contrario que las fechas. Este período es de siete días 
        contrariamente a lo que parece porque el método es static.
             */
            {
                Period wrong = Period.ofYears(1).ofWeeks(1);
                System.out.println("wrong: " + wrong);
            }
            //El ejemplo anterior es igual al siguiente
            {
                Period wrong = Period.ofYears(1);
                wrong = Period.ofWeeks(1);
            }

            /*
        * Si bien un período comprende un día o más, las instancias de la clase Duration pueden contener el
        número de días, horas, minutos, segundos y nanosegundos.
            podrías pasar 365 días para hacer un año.
        * 
        * Ni Duration ni Instant (un instante) están presentes en el examen
        * pero te puede ser útil en tu día a día.
             */
            Duration duration1 = Duration.of(10, ChronoUnit.MINUTES);
            System.out.println("duration1: " + duration1);
            Instant instant1 = Instant.now();
            System.out.println("instant1: " + instant1);

        }
        System.out.println("\n2. Trabajando con Periods \n");
        {
            System.out.println("Agregando meses con long\n");
            {
                LocalDate start = LocalDate.of(2015, Month.JANUARY, 1);
                LocalDate end = LocalDate.of(2015, Month.MARCH, 30);
                LocalDate upTo = start;
                while (upTo.isBefore(end)) {
                    System.out.println("dame un nuevo mes: " + upTo);
                    upTo = upTo.plusMonths(1);
                }
            }
            System.out.println("Agregando meses con period\n");
            {
                LocalDate start = LocalDate.of(2015, Month.JANUARY, 1);
                LocalDate end = LocalDate.of(2015, Month.MARCH, 30);
                Period period = Period.ofMonths(1);
                LocalDate upTo = start;
                while (upTo.isBefore(end)) {
                    System.out.println("dame un nuevo mes: " + upTo);
                    upTo = upTo.plus(period);
                }
                LocalTime time = LocalTime.now();
                //time.plus(period); //java.time.temporal.UnsupportedTemporalTypeException: Unsupported unit: Months
            }

        }
    }
}
