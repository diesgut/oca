package _3_apisnucleojava._6_fechasyhoras;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.Period;
import java.time.temporal.ChronoUnit;

public class Leccion3_Manipulacion_Fechas {

    public static void main(String[] args) {

        System.out.println("\n1. Manipulacion Fechas \n");
        {
            System.out.println("1.1 Plus Days");
            // Manipular fechas con las nuevas clases de Java 8 es fácil.
            LocalDate localDate = LocalDate.of(2014, Month.JANUARY, 20);
            System.out.println("localDate: " + localDate); //2014-01-20
            localDate = localDate.plusDays(2);
            System.out.println("localDate plus 2 days: " + localDate); // 2014-01-22
            localDate = localDate.plusWeeks(1);
            System.out.println("localDate plus 1 week: " + localDate); // 2014-01-29
            localDate = localDate.plusMonths(1);
            System.out.println("localDate plus 1 month: " + localDate); // 2014-02-28
            localDate = localDate.plusYears(5);
            System.out.println("localDate plus 5 years: " + localDate); // 2019-02-28
            // También tenemos la familia de métodos minus...
            localDate = localDate.minus(Period.ofDays(3));
            System.out.println("localDate minus 3 days: " + localDate);
            localDate = localDate.minus(1, ChronoUnit.YEARS);
            System.out.println("localDate minus 1 year: " + localDate);

        }
        System.out.println("\n1.1 Minus Days\n");
        {
            // La clase LocalDate es inmutable, por lo tanto cuidado! este código no
            // tiene ningún efecto:
            LocalDate localDate = LocalDate.of(2014, Month.JANUARY, 20);
            System.out.println("localDate: " + localDate);
            localDate = localDate.minusDays(1);
            System.out.println("localDate minus 1 day: " + localDate); //2014-01-19

        }
        System.out.println("\nLocal Date Time");
        {
            LocalDate date = LocalDate.of(2020, Month.JANUARY, 20);
            System.out.println("date: " + date); //2020-01-20
            LocalTime time = LocalTime.of(5, 15); //05:15
            System.out.println("time: " + time);
            LocalDateTime dateTime = LocalDateTime.of(date, time);
            System.out.println("dateTime: " + dateTime); //2020-01-20T05:15
            dateTime = dateTime.minusDays(1);//
            System.out.println("dateTime 1 minusdays: " + dateTime); //2020-01-20T05:15
            dateTime = dateTime.minusHours(10);
            System.out.println("dateTime minus 10 hours " + dateTime);
            dateTime = dateTime.minusSeconds(30);
            System.out.println("dateTime minus 30 seconds " + dateTime);
        }
        System.out.println("\nEncadenamiento");
        {
            LocalDateTime dateTime = LocalDateTime.now();
            dateTime = dateTime.plusYears(1).minusDays(1).plusHours(22);
        }

    }
}
