package _3_apisnucleojava._2_clasestringbuilder;

/**
 * La clase StringBuilder
 *
 * La diferencia más importante con respecto String es que StringBuilder es
 * mutable, o dicho de otra manera, no es inmutable.
 *
 * La clase StringBuilder se añadió a Java en la versión 5. Anteriormente se
 * usaba StringBuffer que es similar pero más lenta porque es multithread.
 *
 */
public class Leccion1 {

    public static void main(String[] args) {
        /**
         * En este ejemplo, dada la naturaleza inmutable de las cadenas en Java,
         * en cada iteración, cuando se asigna una nueva cadena a la variables
         * alpha, el objeto anterior esta disponible para que el Garbage
         * Collector lo recupere.
         *
         * Como resultado, durante el bucle se crean un total de 27 instancias
         * de la clase String, la mayor parte de las cuales son candidatas para
         * que le Garbage Collector las reclame.
         *
         */
        String alphaString = "";
        for (char current = 'a'; current <= 'z'; current++) {
            alphaString += current;
        }
        System.out.println("alpha String: " + alphaString);
        /**
         * Por su puesto, esto es demasiado ineficiente. La clase StringBuilder
         * es mutable y no guarda los valores intermedios lo que la hace más
         * eficiente.
         *
         * En cada iteración StringBuilder modifica su estado y devuelve una
         * referencia a sí mismo.
         */
        StringBuilder alphaStringBuilder = new StringBuilder();
        for (char current = 'a'; current <= 'z'; current++) {
            alphaStringBuilder.append(current);
        }
        System.out.println("alphaStringBuilder: " + alphaStringBuilder);

        System.out.println("1. Mutabilidad y Encadenamiento");
        /**
         * Cuando asignamos una referencia a otra, ambas apunta al mismo objeto,
         * por lo tanto, el hecho de modificar una, modifica la otra también.
         */
        StringBuilder abc = new StringBuilder("abc");
        //b y a apuntan al mismo objeto: sólo hay un objeto StringBuilder referenciado por dos variables
        StringBuilder defg = abc.append("de");
        defg = defg.append("f").append("g");
        System.out.println("a: " + abc); //abcdefg
        System.out.println("b: " + defg); //abcdefg

        System.out.println("2. Creacion de StringBuilder");
        /*
        * StringBuilder tiene tres constructores que debes conocer:
        * 
        * El contructor por defecto crea un objeto StringBuilder con una capacidad de 16 caracteres.
         */
        StringBuilder sb1 = new StringBuilder(); //secuencia de caracteres vacias
        StringBuilder sb2 = new StringBuilder("animal");
        StringBuilder sb3 = new StringBuilder(10); //cantidad maxima de caracteres
        /*
        * Tamaño y capacidad son diferentes:
        * 
        * Tamaño se refiere al número de caracteres que contiene y capacidad al número de caracteres que puede contener actualmente.
        * En el caso de las cadenas como son inmutables, capacidad y tamaño coinciden.
         */
        System.out.println("sb1.capacity(): " + sb1.capacity());
        System.out.println("sb2.capacity(): " + sb2.capacity());
        System.out.println("sb3.capacity(): " + sb3.capacity());
        System.out.println("sb1.length(): " + sb1.length());
        System.out.println("sb2.length(): " + sb2.length());
        System.out.println("sb3.length(): " + sb3.length());

        System.out.println("3. Metodos String Builder");
        System.out.println("3.1 charAt, indexOf, length y substring");

        /**
         * Funcionan igual que en la clase String.
         *
         * Cuál es la salida de estas línas de código?
         */
        StringBuilder sb4 = new StringBuilder("animals");
        String sub = sb4.substring(sb4.indexOf("a"), sb4.indexOf("al")); //el metodo substring devuelve un string en lugar de stringbuilder
        int len = sb4.length();
        char ch = sb4.charAt(6);
        System.out.println(sub + " " + len + " " + ch); //anim 7 s

        System.out.println("3.2 append");
        /**
         * El método append() es probablemente el método más utilizado de la
         * clase StringBuilder, está sobrecargado para soportar virtualmente
         * cualquier tipo de datos, y no es necesario convertir nustro parametro
         * a string para poder usarlo
         */
        StringBuilder sb5 = new StringBuilder().append(1).append('c');
        sb5.append("-").append(true);
        System.out.println("sb5: " + sb5); // 1c-true

        System.out.println("3.2 insert");
        /**
         * El método insert() añade caracteres a StringBuilder en una posición
         * concreta. está sobrecargado para soportar virtualmente cualquier tipo
         * de datos, y no es necesario convertir nustro parametro a string para
         * poder usarlo
         */
        StringBuilder sb6 = new StringBuilder("Oracle Associate.");
        System.out.println("sb6: " + sb6); //Oracle Associate.
        sb6.insert(7, "Certification ");
        System.out.println("sb6: " + sb6); //Oracle Associate.

        System.out.println("3.3 delete y deleteCharAt");
        /*
        * El método delete() es el opuesto, elimina caracteres de una secuencia y retorna una referencia al StringBuilder actual.
        * 
        * El método deleteCharAt() es conveniente cuando sólo queremos eliminar un caracter.
         */
        sb6.delete(7, 21);
        System.out.println("sb6: " + sb6); //sb6: Oracle Associate.
        sb6.deleteCharAt(sb6.length() - 1);
        System.out.println("sb6: " + sb6); //sb6: Oracle Associate
    }
}
