package _3_apisnucleojava._1_clasestring;

/**
 * La creación y manipulación de cadenas
 *
 * La clase String es una secuencia de caracteres inmutable.
 *
 */
public class Leccion1 {

    public static void main(String[] args) {
        //La clase string es un "tipo de dato de referencia" especial que permite crear variables, con o sin la palabra reservada new
        {
            String s1 = new String("mi cadena 1");
            String s2 = "mi cadena 1";
        }
        System.out.println("1. Concadenacion");
        {
            /**
             * En la seccion de operadores, aprendimos que el operador "+" esta
             * sobrecargado y sirve tanto para sumar numeros como para
             * concadenar cadenas, las reglas del operador "+" son las
             * siguientes:
             */
            //1. Si ambos operandos son numerivos, significa una operacion de suma
            //2. si alguno de los operandos es string, significa una operacion de concadenacion
            //3. Las expresiones son evaluadas de izquierda a derecha
            System.out.println(1 + 2);//3
            System.out.println("a" + "b");//ab
            System.out.println("a" + "b" + 3);//ab3
            System.out.println(1 + 2 + "c");//3c
            System.out.println("a" + "b" + 3 + 2);//ab32
            int tres = 3;
            String cuatro = "4";
            System.out.println(1 + 2 + tres + cuatro); //64
            String s = "1"; //1
            s += "2"; //12
            s += 3;//123
            System.out.println(s); //123

            System.out.println("\"a\" + \"b\": " + "a" + "b"); // "a" + "b": ab
            System.out.println("\"a\" + \"b\" + 3: " + ("a" + "b" + 3)); // Si un operando es de tipo cadena el resultado será una cadena: "a" + "b" + 3: ab3
            System.out.println("1 + 2 + \"c\": " + 1 + 2 + "c"); // La expresión se evalúa de izquierda a derecha: 1 + 2 + "c": 12c

        }
        System.out.println("2. Inmutabilidad");
        {
            //Una string es inmutable lo que significa, que una vez creado no puede ser modificado de ninguna forma.
            /**
             * Una clase es inmutable si solo tiene getter, Esta no tiene forma
             * de cambiar sus valores Las mutables poseen, setter, esto permite
             * que la referenciapueda cambiar sus valores.
             */
            //Las clases inmutables en java, son final, y sus subclases no pueden agregar un comportamiento mutable
            // Un objeto String es inmutable (no puede modificarse) y final.
            String s5 = "a";
            String s6 = s5.concat("b");
            // s6 no se modifica
            s6.concat("c");
            System.out.println("s6: " + s6);

        }
        System.out.println("3. The String Pool");
        // Java sabe que las cadenas ocupan entre el 25% y el 40% de la memoria de un programa, por eso ha creado un área (pool) de cadena donde
        // alberga cadenas repetidas para que no ocupen espacio extra. Estas cadenas van al pool, como son iguales, ocupan la misma

        // Estas cadenas al estar creadas con el operador new no forman parte del pool,
        //sus direcciones de memoria son diferentes.
        String s1 = new String("OCA");
        String s2 = new String("OCA");

        // Estas cadenas si forman parte del pool, y comparten la misma dirección de memoria por lo tanto son iguales.
        String s3 = "OCA";
        String s4 = "OCA";

        System.out.println("s1 == s2: " + (s1 == s2)); // false
        System.out.println("s1 == s3: " + (s1 == s3)); // false
        System.out.println("s3 == s4: " + (s3 == s4)); // true

        String s7 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        // Durante el examen tendrás que demostrar que sabes utilizar los métodos básicos de la clase String.
        System.out.println("s7.length(): " + s7.length());
        // Empieza desde cero
        System.out.println("s7.charAt(5): " + s7.charAt(5));
        System.out.println("s7.indexOf('M'): " + s7.indexOf('M'));
        System.out.println("s7.indexOf(\"M\", 20): " + s7.indexOf("M", 20));
        System.out.println("s7.indexOf(\"ABCD\"): " + s7.indexOf("ABCD"));
        System.out.println("s7.substring(20): " + s7.substring(20));
        System.out.println("s7.substring(5, 7): " + s7.substring(5, 7));
        System.out.println("s7.substring(10, s7.indexOf(\"Q\")): " + s7.substring(10, s7.indexOf("Q")));
        // cadena vacia
        System.out.println("s7.substring(5, 5): " + s7.substring(5, 5));
        // Lanza java.lang.StringIndexOutOfBoundsException:
        // System.out.println(s7.substring(5, 3));
        // Lanza java.lang.StringIndexOutOfBoundsException:
        // System.out.println(s7.substring(5, 30));
        System.out.println("s7.toLowerCase(): " + s7.toLowerCase());
        System.out.println("\"abc\".equals(\"abc\"): " + "abc".equals("abc"));
        System.out.println("\"abc\".equals(\"ABC\"): " + "abc".equals("ABC"));
        System.out.println("\"abc\".equalsIgnoreCase(\"abc\"): " + "abc".equalsIgnoreCase("abc"));
        System.out.println("s7.startsWith(\"ABC\"): " + s7.startsWith("ABC"));
        System.out.println("s7.contains(\"FGHJ\"): " + s7.contains("FGHJ"));
        System.out.println("s7.replace('A', 'a'): " + s7.replace('A', 'a'));
        System.out.println("\"*** \" + \"   abc def      \".trim() + \" ***\": " + "*** " + "   abc def      ".trim() + " ***");
    }
}
