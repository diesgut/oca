/**
 * En el examen no se preguntara si una clase es mutable o no
 * pero siempre es util saber identificarlas
 */
package _3_apisnucleojava._1_clasestring;

class Mutable {

    private String s;

    public void setS(String newS) { //El metodo setter hace que la clase sea mutable
        this.s = newS;
    }

    public String getS() {
        return this.s;
    }
}

class InMutable {

    private String s = "name";

    public String getS() {
        return this.s;
    }
}

/**
 * Una clase es inmutable si solo tiene getter, Esta no tiene forma de cambiar
 * sus valores Las mutables poseen, setter, esto permite que la referenciapueda
 * cambiar sus valores.
 */
//Las clases inmutables en java, son final, y sus subclases no pueden agregar un comportamiento mutable
public class Leccion2_Mutabilidad {

}
