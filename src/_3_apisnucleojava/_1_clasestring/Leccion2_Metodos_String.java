package _3_apisnucleojava._1_clasestring;

/**
 * La creación y manipulación de cadenas
 *
 * Metodos de la clase String.
 *
 */
public class Leccion2_Metodos_String {

    /**
     * Para el examen, solo se tomara en cuenta los metodos mas importantes
     * usados en el mundo readl. Lo que es muy improtante recordar que string es
     * una secuencia de caracteres y java empieza a contar las secuencias desde
     * el indice 0 (cuando se usa indices o posiciones).
     *
     */
    public static void main(String... args) {
        System.out.println("1. length");
        //devuelve el numero de caracteres del string
        String animal = "animal";
        System.out.println(animal.length());//6

        System.out.println("2. charAt");
        //permite buscar que caracter coresponde con determinado indice
        System.out.println(animal.charAt(0));//a
        System.out.println(animal.charAt(5));//l
        //    System.out.println(animal.charAt(6));// error de ejecuion, recordar que java considera los indices desde 0
        //StringIndexOutOfBoundException: String index out of range 7

        System.out.println("3. indexOf");
        //Busca la posicion de un carectar o cadena, dentro de otra cadena
        System.out.println(animal.indexOf('a'));//0
        System.out.println(animal.indexOf("al"));//4
        System.out.println(animal.indexOf('a', 4));//4 //busca el caracter a, a partir de la posicion 4
        System.out.println(animal.indexOf("al", 5));//-1

        System.out.println("4. substring()");
        //crea una subcadena, a partir de otra cadena, tomando como indice inicial el primer argumento del metodo
        // hasta el final de la cadena principal, o finaliza en el indice del segundo argumento opcional (sin incluirlo).
        animal = "animal";
        System.out.println(animal.substring(1)); //nimal
        System.out.println(animal.substring(animal.indexOf("i"))); //imal
        System.out.println(animal.substring(4, 4));//vacio
        System.out.println(animal.substring(4, 5));//a
        System.out.println(animal.substring(4, 6));//se permite que el indice final, sea uno mas pasado el final de la secuencia
        // System.out.println(animal.substring(4, 3)); throws exception
        // System.out.println(animal.substring(4, 7)); throws exception

        System.out.println("5. toLowerCase and toUppserCase");
        //estos metodos hacen exactamnto lo que su nombre dice, y solo afectan caracteres
        //recorcar que crea una nueva cadena, la cadena original sigue igual
        System.out.println(animal.toUpperCase()); //ANIMAL
        System.out.println("Abc123".toLowerCase()); //abc123

        System.out.println("5. equals and equalIgnoreCase");
        //comprueba que dos cadenas tengan exactamente los mismos caracteres y en el mismo orden
        //ignoreequals no comprueba si estan en mayusculas o minusculas
        System.out.println("abc".equals("ABC")); //false
        System.out.println("ABC".equals("ABC")); //true
        System.out.println("abc".equalsIgnoreCase("ABC")); //true

        System.out.println("6. startsWith & endsWith");
        //evalua que una cadena empiece o termine con con el valor del argumento
        System.out.println("abc".startsWith("a")); //true
        System.out.println("abc".startsWith("A")); //false
        System.out.println("abc".endsWith("c")); //true
        System.out.println("abc".endsWith("C")); //false

        System.out.println("7. contains");
        //similar a startWith y endWith, solo que busca el contenido en cualquier parte de la cadena
        System.out.println("abc".contains("b")); //true
        System.out.println("abc".contains("B")); //false

        System.out.println("8. Replace");
        //Este metodo busca recibe como primer argumento una interfaz CharSequence (interfaz para representar cadenas y stringbuilder)
        //y lo remplaza por el valor de su segundo arguento que tambien es un CharSequence
        System.out.println("abcabc".replace('a', 'A')); //AbcAbc
        System.out.println("abcabc".replace("a", "A")); //AbcAbc

        System.out.println("9. Trim");
        //Remueve los espacioes en blanco de la cadenas, \t tabs and \n newlines, en la izquierda y derecha
        System.out.println("abc".trim()); //abc
        System.out.println("\t a b c\n");
        /*
            a b c
            
         */
        System.out.println("\t a b c\n".trim()); //a b c, removio el tab y el salto de linea

        System.out.println("10. Encadenamiento de Metodos");
        
        String start = "AniMal   ";
        String trim = start.trim(); //AniMal
        String lowerCase = trim.toLowerCase();// animal
        String result = lowerCase.replace('a', 'A'); //Animal
        //Cada vez que un metodo es llamado, se pone en una nueva variable
        //los pasos anteiores pueden resumirse de la seguiente forma
        result = "AniMaL   ".trim().toLowerCase().replace('a', 'A');
        //Este metodo es similar al anterior, cada vez que se llama a un metodo el nuevo valor de retorno, es pasado al otro metodo
        System.out.println(result);///Animal
        /////
        String a = "abc";
        String b = a.toUpperCase();
        b = b.replace("B", "2").replace('C', '3');
        System.out.println("a=" + a);//abc
        System.out.println("b=" + b);//A23

    }

}
