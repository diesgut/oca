package _4_metodosyencapsulacion._8_expresioneslambda;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

/**
 * @author pep
 *
 * Las expresiones lambda simples
 *
 * En esencia Java es un lenguaje orientado a objetos, pero Java 8 añadio la
 * posibilidad de escribir codigo al estilo funcional, mas declarativo.
 *
 * La programacion funcional se centra en el Que queremos hacer y no en el Como.
 *
 * La programacion funcional utiliza expresiones lambda que se pueden
 * interpretar como metodos anonimos ya que tiene parametros y cuerpo pero no un
 * nombre como un verdadero metodo.
 *
 * Las expresionres lambda o simplemente lambdas se las conoce como closures en
 * otros lenguajes, aunque Java las ha simplificado enormemente.
 *
 * El examen OCA tan solo revisa conceptos fundamentales de las expresiones
 * lambda que se ven en profundidad en el examen Oracle Certified Professional
 * (OCP).
 *
 * De cara al examen OCA necesitas conocer la interfaz Predicate, una interfaz
 * funcional porque define un unico metodo abstracto test.
 *
 * De todas formas, te recomiendo que dediques a las expresiones lambda,
 * referencias a metodos, interfaces funcionales y streams algun tiempo porque
 * cada vez van a estar mas presentes.
 *
 */
public class Leccion1 {

    public static void main(String[] args) {

        // Asi se definia una interfaz antes
        Predicate<String> p1 = new Predicate<String>() {
            @Override
            public boolean test(String s) {
                return s.startsWith("A");
            }
        };
        /*
	* Este codigo define una instancia de Predicate con un expresion lambda y es equivalente al anterior. 
        Como ves las lambdas son muy compactas.
         */
        Predicate<String> p2 = s -> s.startsWith("A");
        /*
	* Java 8 declara multiples metodos donde se pueden aplicar predicados,  
        pero para el examen solo necesitas conocer uno, el metodo removeIf de la clase ArrayList.
         */
        List<String> lista1 = Arrays.asList("ABCDMX", "ABCX", "ABCD", "FGHX",
                "DFMTU");
        lista1 = new ArrayList<String>(lista1);
        /*
	* Este sencillo ejemplo define una expresion lambda que implementa Predicate. 
	* A continuacion el metodo removeIf la utiliza para elimitar cadenas de la lista, que empiezan por A
         */
        lista1.removeIf(p2);
        System.out.println(lista1);

    }

}
