/**
 * Alfa.java
 */
package _4_metodosyencapsulacion.code.p1;

/**
 *
 * Desde la clase Alfa todos los miembros son accesibles. Fíjate que cada
 * miembro tiene un modificador de acceso diferente de más restrictivo a menos.
 *
 */
public class Alfa {

    private void f() {
    }

    void g() { //default
    }

    protected void h() {
    }

    public void i() {
    }

    //
    public final void j() {
        System.out.println("j");
    }

    public Alfa() {
        f();
        g();
        h();
        i();
    }
}
