/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package _4_metodosyencapsulacion._4_datosentremetodos;

import java.math.BigDecimal;

/*
Java es un lenguaje que pasa los argumentos de las llamadas a métodos por valor, 
lo que significa que los métodos reciben una copia de las variables, 
es decir si se asigna un nuevo valor a un parámetro no tiene ningún efecto sobre la variable original 
(Solo tipos inmutables).

 */
public class Leccion1 {

    int variableClase;

    /*
	 * Los cambios hechos en i no afectan al argumento de llamada. Si quieres prevenir cambios en i, 
    puedes declarar el argumento como final int i.
     */
    static void m1(int i) {
        i++;
    }

    /*
    * La clase String es una clase inmutable, por lo tanto los cambios hechos en un cadena no afectan 
    al argumento de llamada.
     */
    static void m2(String s) {
        s = s.toUpperCase();
    }

    /**
     * ********
     * La clase StringBuilder es una clase mutable, por lo tanto los cambios
     * hechos en un objeto StringBuilder Si afectan al argumento de llamada.
     */
    static void m3(StringBuilder sb) {
        sb.reverse();
    }

    /*
    * En cambio, si cambiamos el objeto al que apunta, el argumento de llamada no se ve afectado.
     */
    static void m4(StringBuilder sb) {
        sb = new StringBuilder("XXX");
    }

    static void manejoBigDecimal(BigDecimal contador) {
        contador = contador.add(new BigDecimal(2));
        System.out.println("valor bigdecimal en el metodo " + contador);
    }

    /**
     * ********
     * @param args
     */
    public static void main(String[] args) {

        /*
		 * Cual crees que sera la salida de este codigo?
         */
        int k = 1;
        System.out.println("k antes de invocar a m1(): " + k);
        m1(k);
        System.out.println("k despuÃ©s de invocar a m1(): " + k);
        String str = "oca";
        System.out.println("str antes de invocar a m2(): " + str);
        m2(str);
        System.out.println("str despuÃ©s de invocar a m2(): " + str);
        StringBuilder builder1 = new StringBuilder("OCA");
        System.out.println("builder1 antes de invocar a m3(): " + builder1);
        m3(builder1);
        System.out.println("builder1 despuÃ©s de invocar a m3(): " + builder1);
        StringBuilder builder2 = new StringBuilder("OCA");
        System.out.println("builder2 antes de invocar a m4(): " + builder2);
        m4(builder2);
        System.out.println("builder2 despuÃ©s de invocar a m4(): " + builder2);
        BigDecimal valorBigDecimal = new BigDecimal(1);
        System.out.println("valor bigdecimal antes del metodo " + valorBigDecimal);
        manejoBigDecimal(valorBigDecimal);
        System.out.println("valor bigdecimal despues del metodo " + valorBigDecimal);
    }

}
