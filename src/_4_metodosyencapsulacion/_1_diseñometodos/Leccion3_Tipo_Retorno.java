package _4_metodosyencapsulacion._1_diseñometodos;

public class Leccion3_Tipo_Retorno {

    {
        System.out.println("3. Tipo de Retorno");
        /**
         * el tipo de retorno podria ser un tipo string o int, si el metodo no
         * tiene un tipo de retorno entonces se usara la palabra reservada void.
         * Los metodos siempre deben tener un tipo de retorno, en el caso de no
         * dovolver nada, se debe retornar el tipo void
         */
    }

    public void walk1() {
        //el tipo de retorno es void y la sentencia return es opcional
    }

    public void walk2() {
        //la sentencia return es correcta y no retorna nadaS
        return;
    }

    public String walk3() {
        //el tipo de retorno es string valido y debe retornar un string
        return "";
    }


    /*
    public String walk4() {
        //NO COMPILA, por que tiene un tipo de retorno string y no esta retornando nada
    }
     */
 /*
    public walk5(){
        //NO COMPILA, por que no tiene tipo de retornoS
    }
     */
 /*
    String walkg6(int a) {
        //NO COMPILA, por que el tipo de retorno es string y el compilador dee detectar que siempre se retornara un string
        if (a == 4) 
            return "";
    }
     */
}
