package _4_metodosyencapsulacion._1_diseñometodos;

public class Leccion5_Parameter_List {

    /**
     * Aunque la lista de parametros es requerida, esta puede estar vacia. Em
     * caso de contener parametros, estos deben estar separados por coma,
     */
    public void walk1() {
        //correcta declaracion de metodo sin parametros
    }

    //public void walk2{} no compila, por que no tiene parentesis que encierren los parametros
    public void walk3(int a) {
        //metodo valido con un parametro
    }

    //public void walk4(int a; int b){} no compila, por que los parametros van separados por coma
    public void walk5(int a, int b) {
        //correcta declaracion de metodos, con parametros separados por coma
    }
}
