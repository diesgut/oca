package _4_metodosyencapsulacion._1_diseñometodos;

public class Leccion2_Especificador_Opcional {

    {
        System.out.println("2. Especificador Opcionales");
        /**
         * static vuelve a una variable en variable de clase y aun metodo en
         * metodo de clase.
         *
         * abstract usado cuando no se define el cuerpo del metodo y sera
         * sobreescrito
         *
         * syncrinized se vera en el examen OCP
         *
         * native usado cuando se interactua con codigo escrito en otro
         * lenguaje.
         *
         * stricftp usado para hacer calculos con punto flotante,
         *
         *
         */
    }

    public void walk1() {
        //metodo sin especificador opcional
    }

    public final void walk2() {
        //metodo con el especificador opcional final
    }

    public static final void walk3() {
        //  static final public void walk3() valid
        //metodo con los especificadores opcionales static y final
    }

    public final static void walk4() {
        //  final static public void walk4() valid
        //metodo con los especificadores opcionales static y final
    }

    /*
    public modifier void walk5(){
     //no compila por que modifier no es un especificador opcional valido
    }
     */
 /* 
    public void final walk6(){
    //no compila por que el modificador opcional va antes del tipo de retorno
    }
     */
    final public void walk7() {
        //valida puesto que se permite que los especificadores puedan ir antes de los modificadores de acceso
    }
}
