package _4_metodosyencapsulacion._1_diseñometodos;

import java.util.List; // No se pueden importar dos clases con el mismo nombre.
// import java.awt.List;

import static java.util.Arrays.asList;
import static java.util.Arrays.*; //Aunque usando comodines es posible importar implicitamente un metodo mas de una vez!!!.

/**
 *
 * Para acceder a los miembros (propiedades y metodos) estaticos de una clase no
 * se requiere una instancia. Por eso se llaman miembros de clase, en oposicion
 * a los miembros de instancia.
 *
 * Todos los miembros estaticos son compartidos entre todos los usuarios de la
 * clase, como si existieran independientemente de cualquier instancia de la
 * clase.
 *
 * Cada instancia de una clase tiene su propia copia de las variables de
 * instancia (no static) pero solo hay una copia de las variables de clase
 * (static) que es compartida por todas las instancias de la clase. En cuanto a
 * los metodos, tanto de instancia como de clase solamente hay una copia a
 * efectos de eficiencia, y los parametros y variables locales se situan en la
 * pila.
 *
 * El metodo main, que hemos usado ampliamente, es publico y estatico con el
 * objetivo de que sea accesible desde cualquier parte y que el cargador de
 * clases no necesite crear una instancia de la clase para invocarlo.
 *
 * Aparte del metodo main, declaramos miembros estaticos en varios escenarios:
 *
 * 1. Cuando queremos definir metodos de utilidad, como hace la clase Math.
 *
 * 2. Cuando queremos definir estado que sea compartido por todas las instancias
 * de una clase. Por ejemplo, el numero de instancias que tiene un clase.
 *
 * Y RECUERDA, LOS METODOS ESTATICOS SOLO PUEDEN ACCEDER A OTROS MIEMBROS
 * ESTATICOS. EN CAMBIO, LOS METODOS DE INSTANCIA (NO ESTATICOS) PUEDEN ACCERDER
 * TANTO A MIEMBROS DE CLASE (ESTATICOS) COMO DE INSTANCIA (NO ESTATICOS).
 *
 */
class DemoStatic {

    public static int count = 0;
    private int i = 0;

    public DemoStatic() {
        count++;
    }

    public static void m1() {
        // System.out.println(i); // No compila, un miembro static no puede acceder a miembros de instancia.
        System.out.println(count);
    }

    public void m2() {
        System.out.println(i);
        System.out.println(count);
    }
}

class DemoStaticInit {

    /*
    * En cuanto a las constantes estaticas o bien se inicializan cuando se declaran o bien en un bloque estatico
    (solo una vez), de otra forma se genera un error de compilacion.
     */
    private static final int MAX_ELEM;
    private static final int CONST1 = 0;
    // private static final int CONST2; // No compila, no esta¡ inicializada

    /*
    * Los bloques de inicializacion estaticos se ejecutan cuando la clase se usa por primera vez.
    * 
    * Cuando se crea una instancia de esta clase, primero se ejecuta el codigo de los bloques estaticos 
    (en orden de aparicion) y despues el codigo de los bloques de instancia
    * (tambien en orden).
    * 
    * De todas formas, se recomienda no utilizar bloques de inicializacion de instancia y trasladar ese codigo 
    al constructor.
    * 
     */
    static { //bloque estatico de incicializacion
        System.out.println("En bloque estÃ¡tico");
        int max_filas = 3;
        int max_cols = 5;
        MAX_ELEM = max_filas * max_cols;
    }

    { //bloque de instancia estatico de incicializacion
        System.out.println("En bloque de instancia");
    }

    public static void m1() {
        System.out.println(MAX_ELEM);
    }

}

public class Leccion2 {

    public static void main(String[] args) {
        DemoStatic.m1();
        DemoStatic ds1 = new DemoStatic();
        DemoStatic ds2 = new DemoStatic();
        DemoStatic ds3 = new DemoStatic();
        DemoStatic.m1();
        ds1.m1();
        ds2.m1();
        ds3.m1();
        ds1 = null;
        /*
	* ds1 es todavia un objeto DemoStatic
	* 
	* Totalmente vÃ¡lido para invocar un mÃ©todo estÃ¡tico!!!
         */
        ds1.m1();
        /*
	 * Como invocamos un meodo estatico, solamente el bloque estÃ¡tico se ejecuta, pero solo una vez.
         */
        DemoStaticInit.m1();
        DemoStaticInit.m1();
        /*
	 * En este caso creamos una instancia de la clase DemoStaticInit, por lo tanto, si no se ha ejecutado ya, 
	* se invoca el codigo de inicializacion estatico primero y despues el codigo de inicializacion de instancia.
         */
        DemoStaticInit dsi = new DemoStaticInit();
        /*
	* En cuanto a las ordenes import, recuerda, asÃ­ como los imports normales importan clases, 
        los imports estaticos importan miembros estaticos (como metodos) de clases concretas.
         */
        List<String> list1 = asList("alfa", "bravo");
        /*
	* Fiate bien que el import estatico importa el metodo asList y no la clase Arrays.
         */
        // List<String> list2 = Arrays.asList("alfa", "bravo"); // No compila
    }

}
