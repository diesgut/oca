package _4_metodosyencapsulacion._1_diseñometodos;

public class Leccion4_Nombre_Metodo {

    /**
     * Los nombres de metodos siguen las mismas reglas del nombre de variables,
     * para recordar un identificador puede contener letras, numeros, $ o _.
     * Ademas que el primer caracter no puee ser numero. y existen palabras
     * claves que no peden ser usadas.
     */
    public void walk1() {
    }

    //public void 2walk1(){} no compila
    //public walk3 void(){} no compila, el nombre del metodo va antes del tipo de retorno
    //public walk3 void(){} no compila
    public void Walk_3() {
        //si compila pero por convencion, la primera letra siempre deberia ser minuscula
    }

    // public void(){} no compila, no tiene nombre de metodo
}
