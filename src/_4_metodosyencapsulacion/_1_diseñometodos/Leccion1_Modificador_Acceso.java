package _4_metodosyencapsulacion._1_diseñometodos;

import _4_metodosyencapsulacion.code.p1.Alfa;

public class Leccion1_Modificador_Acceso {

    {
        System.out.println("1. Modificadores de Acceso");
        /**
         *
         * Aplicando los modificadores de acceso (private, protected, default y
         * public ) (Improtante para el examen)
         *
         * los miebros public son accesibles desde cualquier clase en cualquier
         * paquete
         *
         * Los miembros private sólo son accesibles desde la propia clase.
         *
         * Los miembros protected son accesibles desde la propia clase, desde el
         * mismo paquete y desde las subclases.
         *
         * Los miembros default son accesibles desde la propia clase y desde el
         * mismo paquete. no lleva el modificador puesto que se sobreentiende
         *
         */

    }

    public void walk1() {
        //metodo valido con acceso publico
    }

    //default walk2(){}//no compila por q la palabra default no es un modificador de acceso
    //void public walk3(){} no compila por que el modificador de acceso va antes del tipo de retorno
    void walk4() {
        //metodo con acceso por defecto
    }

    /*
	 * Esta es una de las partes más duras del examen. Asegurate de que la trabajas a fondo, revisando estos ejemplos hasta que los entiendas
	 * completamente, respondiendo corectamente a las preguntas de los tests y haciendos tus propios ejemplos.
	 * 
	 * Es esta lección vamos a usar las clases Alfa y Bravo declaradas en este paquete: es.smartcoding.oca.seccion4.code.p1 
	 * y la clase Charlie que extiende Alfa declarada en este paquete es.smartcoding.oca.seccion4.code.p2.
	 * 
	 * También vamos a ilustrar el tema de los miembros estáticos con dos clases: la clase DemoStatic y DemoStaticInit.
     */
    public static void main(String[] args) {
        Alfa alfa = new Alfa();
        /*
		 * private, sólo es accesible desde la propia clase.
         */
        // alfa.f(); // No compila
        /*
		 * default, sólo es accesible desde la propia clase y el paquete donde
		 * está definida.
         */
        // alfa.g(); // No compila
        /*
		 * protected, sólo es accesible desde la propia clase, el paquete donde
		 * está definida y subclases.
         */
        // alfa.h(); // No compila
        /*
		 * public, accesible desde cualquier parte
         */
        alfa.i();

    }
}
