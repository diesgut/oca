package _4_metodosyencapsulacion._7_encapsulaciondatos;

/**
 *
 * La encapsulacion de datos
 *
 * Previene accesos indeseables al estado de los objetos. Es considerado uno de
 * los pilares de la programacion orientada a objetos, junto a la herencia y
 * polimorfismo.
 *
 * La encapsulacion previene que se pueda manipular las propiedades de un objeto
 * a nuestro antojo.
 *
 * La solucion pasa por declarar las propiedades como private y definir metodos
 * accesores public (getters y setters) segun convenga. Si queremos que una
 * propiedad sea de solo lectura, definiremos solamente su metodo getter y si
 * queremos que sea de solo escritura definiremos el metodo setter. De la misma
 * manera si queremos que una propiedad sea de lectura y escritura definiremos
 * ambos metodos getter y setter.
 *
 * Java define los JavaBeans como clases con una serie de caracteristicas:
 *
 * 1. Tienen constructor por defecto.
 *
 * 2. Las propiedades son privadas.
 *
 * 3. Los metodos getter empiezan con 'get' excepto cuando se trata de
 * propiedades booleanas (boolean) que empiezan por 'is'.
 *
 * 4. Los metodos setter empieza con 'set'.
 *
 * 5. Despues del prefijo get, is o set le sigue el nombre de la propiedad con
 * la primera letra en mayusculas (CamelCase).
 */
class CuentaCorriente {
    //double saldo;

    private double saldo;

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

}

public class Leccion1 {

    public static void Leccion1(String[] args) {
        CuentaCorriente cc1 = new CuentaCorriente();
        // cc1.saldo = 1000000;

    }

}
