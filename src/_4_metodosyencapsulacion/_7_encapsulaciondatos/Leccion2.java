package _4_metodosyencapsulacion._7_encapsulaciondatos;

/**
 *
 * La encapsulacion de datos
 *
 * Las clases inmutables tienen una caracteristica tan simple como util. Una vez
 * creadas no cambian su estado, lo que nos garantiza que podemos 'pasearlas'
 * por cualquier parte de la aplicacion sin que cambie su estado.
 *
 * La forma de conseguirlo es igualmente simple, basta con declarar las
 * propiedades como private y no proporcionar ningun metodo setter. La
 * inicializacion se llevara a cabo exclusivamente a traves del constructor.
 *
 */
class MiClaseInmutable {

    private int codigoSecreto;

    public MiClaseInmutable(int codigoSecreto) {
        this.codigoSecreto = codigoSecreto;
    }

    @Override
    public String toString() {
        return String.format("MiClaseInmutable [codigoSecreto=%s]",
                codigoSecreto);
    }

}

/*
 * Lamentablemente estas condiciones a menudo no son suficientes, no siempre es el caso: 
 * la clase MiClaseInmutableFalsa a pesar de no tener ningun metodo setter y tener su propiedad privada, no es
 * inmutable. El problema esta en el metodo getter retorna una referencia a StringBuilder que no es inmutable. 
 * Por lo tanto cualquier cambio en esa referencia modificara la
 * propiedad original.
 * 
 * La solucion es doble: 
 * 1) no retornar la instancia original sino una copia, lo que se llama una copia defensiva.
 * 
 * 2) Otra solucion podria ser retornar un objeto String que es inmutable.
 */
class MiClaseInmutableFalsa {

    private StringBuilder codigoSecreto;

    public MiClaseInmutableFalsa(String str) {
        this.codigoSecreto = new StringBuilder(str);
    }

    public StringBuilder getCodigoSecreto() {
        return codigoSecreto;
        // copia defensiva
        //return new StringBuilder(codigoSecreto);
    }

    // public String getCodigoSecreto() {
    // return codigoSecreto.toString();
    // }
    @Override
    public String toString() {
        return String.format("MiClaseInmutableFalsa [codigoSecreto=%s]",
                codigoSecreto);
    }

}

public class Leccion2 {

    public static void main(String[] args) {
        MiClaseInmutable mci = new MiClaseInmutable(238176322);
        System.out.println(mci);
        MiClaseInmutableFalsa mcif = new MiClaseInmutableFalsa("12345657");
        System.out.println(mcif);
        StringBuilder sb = mcif.getCodigoSecreto();
        sb.setCharAt(3, 'X');
        System.out.println(mcif);
    }

}
