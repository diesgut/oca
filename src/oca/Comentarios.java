package oca;

public class Comentarios {
    
    public String nombre;

    public static void main(String[] args) {
        System.out.println("Comentario una linea");
        //Comentario simple, todo lo escrito despues de los dos slash es ignorado por el compilador

        System.out.println("Comentario multi linea");
        /* empieza con este simbolo
    Comentario multi linea, empieza con el simbolo 
    termina con este simbolo */

 /*
        * Para facilitar la lectura se coloca un * en cada linea 
        * otra linea
         */
        System.out.println("Java Doc");

        /**
         * comienzo Los comentarios de javadoc no son tratados en el examen,
         * pero es bueno tenerlos en cuenta
         *
         * @author diego fin
         */
        System.out.println("Casos multilinea");

        /*
        /* /* podemos volver a colocar el simbolo de apertura /* en cualquier momento
       pero no podemos volver a colocar el simbolo de cierre, o se generará un error de sintaxis. 
       
         */
        System.out.println("Caso una linea");

        //grau
        //larco //larco podemos colocar el simbolo de apertura del comentario de una linea en cualquier momento
        ////san martin
        System.out.println("Casos mixtos"); //
        // /* si intentamos colocar los simbolos de comentario multinea en la linea que empieza // sera considerado como comentario de una linea  */  

        /* Ancash */ // esto tambin funciona
        /*
        El simbolo de comentario simple es ignorado al cerrar el comentario multiple
       //  */
      
    }
}
