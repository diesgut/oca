package oca;

public class Main {
    
    public static void main(String[] args) {
        System.out.println("el modificador static puede ir antes del modificador public");
        if (args != null) {
            for (int i = 0; i < args.length; i++) {
                System.out.println(String.format("Argumento %s %s", i, args[i]));
            }
            
        }
        //podemos compilar el archivo en la linea de comandos con el comando javac Main.java
        //y ejecutar el archivo compilado con el comando java Main 
        //y pasar argunemtos de la siguiente forma java Main java Main hola diego de m "otro argumento mas"
        Main main = new Main();
    }
    
    public Main() {
        Comentarios comentarios = new Comentarios();
        comentarios.nombre = "digo";
        System.out.println("nombre antes metodo " + comentarios.nombre);
        this.metodComentario(comentarios);
        System.out.println("nombre luego metodo " + comentarios.nombre);
    }
    
    public void metodComentario(Comentarios comentarios) {
        comentarios.nombre = "alberto";
        System.out.println("nombre en mtodo " + comentarios.nombre);
    }
    
}
