package _5_diseñoclases._2_clasesabstractas;

/**
 *
 * Las clases abstractas
 *
 * Las clases abstractas son clases incompletas, pensadas para ser extendidas y
 * por lo tanto completadas mediante clases concretas.
 *
 * En otras palabras, abstract y final son dos conceptos totalmente opuestos:
 * una clase o metodo abstracto no puede ser final. De la misma manera, un
 * metodo abstracto no puede ser privado porque no sera visible en la subclases.
 *
 * Una clase que extiende una clase abstracta esta obligada a implementar todos
 * los metodos abstractos de la clase abstracta o a definirse ella misma como
 * abstracta.
 *
 * Estas son las reglas de definicion de una clase abstracta:
 *
 * 1. Una clase abstracta no puede instanciarse directamente.
 *
 * 2. Una clase abstracta puede tener cero o mas metodos abstractos.
 *
 * 3. Una clase abstracta no puede ser final.
 *
 * 4. Una clase abstracta que extiende otra clase abstracta hereda todos sus
 * metodos abstractos.
 *
 * 5. La primer clase concreta que extiende una clase abstracta debe
 * proporcionar una implementacion de todos los metodos abstractos que hereda.
 *
 * Y estas las reglas de definicion de un meodo abstracto:
 *
 * 1. Un metodo abstracto solo puede ser declarado en una clase abstracta.
 *
 * 2. Un metodo abstracto no puede ser declarado como final o private.
 *
 * 3. Los metodos abstractos no pueden tener cuerpo.
 *
 * 4. La implementacion de un metodo abstracto en una clase concreta sigue las
 * mismas reglas de sobrescritura de metodos en cuanto a nivel de visibilidad,
 * firma y gestion de excepciones.
 *
 * 5. Para acabar, una clase abastracta puede tener metodos abstractos y no
 * abstractos.
 *
 *
 */
abstract class Figura {

    // Una clase solo puede tener metodos abstractos si es una clases abstracta. 
    public abstract double area();
}

class Cuadrado extends Figura {

    private double lado;

    public Cuadrado(double lado) {
        this.lado = lado;
    }

    @Override
    public double area() {
        return lado * lado;
    }

}

public class Leccion1 {

    public static void main(String[] args) {
        // No compila, de una clase abstracta no pueden crerse instancias
        // Figura figura = new Figura();
        Figura cuadrado = new Cuadrado(3.0e0);
        System.out.println(cuadrado.area());
    }

}
