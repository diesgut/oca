package _5_diseñoclases._3_interfaces;

/**
 *
 * La implementacion de interfaces
 *
 * Aunque Java no permite la herencia multiple, si que permite a una clase
 * implementar cualquier numero de interfaces.
 *
 * Una interfaz es una estructura de datos abstracta que define una lista de
 * cero o mas metodos publicos y abstractos que cualquier clase concreta debe
 * implementar.
 *
 * Una interfaz es abstract implicitamente y tambien puede contener una lista de
 * cero o mas constantes public, final y static, y Java 8 introduce los metodos
 * por defecto (default methods), con cuerpo y metodos estaticos.
 *
 * Las reglas para definir una interfaz son muy parecidas a las reglas de las
 * clases abstractas:
 *
 * 1. Una interfaz no puede instanciarse directamente.
 *
 * 2. Una interfaz puede tener cero o mas metodos.
 *
 * 3. Una interfaz no puede ser final.
 *
 * 4. Las interfaces definidas a alto nivel (en su propio fichero) pueden ser
 * public o default y se asume abstract.
 *
 * 5. Implicitamente, todos los metodos de una interfaz son abtract y public,
 * por lo tanto un metodo no puede ser private, protected ni final.
 *
 * Importante, ni las clases ni las interfaces internas forman parte de los
 * objetivos del examen OCA.
 *
 * Puedes encontrarte con un escenario donde una clase implementa dos interfaces
 * que definen el mismo meodo por defecto. Este es un caso particular y solo
 * compilara si la clase concreta implementa el metodo por defecto, lo que
 * elimina la ambiguedad. Ademas, los metodos estaticos de una interfaz no se
 * heredan.
 *
 *
 */
abstract interface Vehiculo {

    /*
	 * Las constantes de una interfaz son public, static y final implicitamente.
     */
    public static final int MAX_PUERTAS = 5;

    /*
	 * Los metodos de una interfaz son public y abstract implicitamente.
     */
    public abstract void arranca();

    void gira();

    /*
	 * Los metodos default permiten añdir metodos a interfaces (solamente a interfaces) sin romper la compatibilidad 
    con el codigo antiguo.
	 * 
	 * Imagina que quieres añadir uno o mas metodos a una interfaz que esta siendo implementada por multiples clases 
    sin que se rompa el codigo. La solucion
	 * es añadir metodos por defecto que ya tienen una implementacion.
	 * 
	 * Los metodos por defectos no pueden ser ni abstract ni final ni static.
     */
    public default void frena() {
        System.out.println("ImplementaciÃ³n por defecto de frena...");
    }
}

abstract class Utilitario implements Vehiculo {

    /*
    * Una clase abstracta o interfaz puede redefinir un metodo por defecto como abstracto. A partir de este momento, 
    las subclases deben proporcionar una
    * implementacion para dicho metodo.
     */
    public abstract void frena();
}

class Turismo extends Utilitario {

    @Override
    public void arranca() {
        System.out.println("Arrancando turismo...");

    }

    @Override
    public void gira() {
        System.out.println("Girando turismo...");
    }

    @Override
    public void frena() {
        System.out.println("Frenando turismo...");
    }

}

interface MiInterfaz1 {

    public static void g() {
    }

    public default void f() {

    }
}

interface MiInterfaz2 {

    public static void g() {
    }

    public default void f() {

    }
}

/*
 * La clase MiClase implementa las interfaces MiInterfaz1 y MiInterfaz2. Ambas definen un metodo statico g() 
y un metodo default f(). Por lo tanto esta obligada a 
 * definir su propia version del metodo default f() para eliminar la ambiguedad. De la misma forma recuerda que 
los metodos estaticos no se heredan por lo tanto
 * la invocacion de g() no compila y tampoco compilara si el metodo estatico g() estuviera presente solo en una interfaz.
 */
class MiClase implements MiInterfaz1, MiInterfaz2 {

    public void f() {
        MiInterfaz1.g();
        MiInterfaz2.g();
        //g(); // No compila
    }
}

public class Leccion1 {

    public static void main(String[] args) {
        Turismo turismo = new Turismo();
        turismo.arranca();
        turismo.gira();
        turismo.frena();
    }

}
