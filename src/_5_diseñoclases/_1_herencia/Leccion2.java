package _5_diseñoclases._1_herencia;

import java.util.ArrayList;
import java.util.List;

/**
 * @author pep
 *
 * La herencia
 *
 * La anotacion @Override indica que estamos sobrescribiendo un metodo de la
 * clase padre.
 *
 * Por ejemplo, la clase Barco sobrescribe el metodo toString de la clase
 * java.lang.Object para adecuarlo a sus necesidades particulares.
 *
 * Durante la sobrescritura de metodos el compilador hace las siguientes
 * comprobaciones:
 *
 * 1. Ambos metodos, el sobrescrito (original) y el que sobrescribe (de la
 * subclase) deben tener la misma firma (signature) y valor de retorno.
 *
 * 2. El metodo de la subclase debe ser tan accesible o mas que el metodo
 * sobrescrito.
 *
 * 3. El meodo de la subclase no puede lanzar una excepcion comprobada (checked
 * exception) que sea nueva o mas amplia que las lanzadas en el metodo
 * sobrescrito.
 *
 * 4. Si el metodo original retorna un valor, el metodo de la subclase debe
 * retornar un valor del mismo tipo o un tipo de retorno covariante (una
 * sublcase).
 *
 * Un metodo escondido es un metodo sobrescrito que ademaas es estatico en ambas
 * clases. Si intentas sobrescribir un metodo de clase (estatico) con un metodo
 * de instancia (no estatico) o un metodo de instancia con un metodo de clase
 * (estatico) no compilara.
 *
 * Reusar un nombre de metodo estatico NO es una buena practica.
 *
 * Atencion, no confundas overloading con overriding, aunque ambos redefinen un
 * metodo que tiene el mismo nombre, overloading o sobrecarga varia la firma de
 * un metodo (lista de parametros) y tambien puede modificar el valor de
 * retorno. Pero overriding o sobrescritura tan solo redefine un metodo de la
 * superclase.
 *
 * Los metodos finales, por definicion, no pueden sobrescribirse ni esconderse
 * si es estatico pero si sobrecargarse.
 *
 * En general, definiremos un metodo como final cuando queramos asegurarnos de
 * que no se cambia su comportamiento en ninguna subclase.
 *
 * Finalmente, las reglas para con los nombres de propiedades iguales son mas
 * simples: en Java las variables con igual nombre esconden las variables de la
 * superclase. De manera que para acceder a una variable escondida de la
 * superclase, lo podemos hacer con la palabra reservada super.
 *
 * De todas formas no es una practica recomendada utilizar los mismos nombres de
 * propiedades publicas, propected o default en varios niveles de la jerarquia
 * de clases. Las propiedades private en cambio, se consideran menos
 * problematicas dado que no son visibles en las subclases.
 */
/**
 * ********
 * Clase Base
 */
class Base {

    public int var = 1;

    public static void s() {
        System.out.println("En Base");
    }

    public void f() {
        System.out.println("En Base");
    }

    public List<String> g() throws Exception {
        return null;
    }

    public void h() throws IllegalAccessException {
    }

    /*
	 * Este metodo es privado, no es visible en las subclases
     */
    private void i() {
    }
}

/**
 * ********
 * Clase Subclase
 */
class Subclase extends Base {

    public int var = 2;

    public static void s() {
        System.out.println("En Subclase");
    }

    // No compila, modificador de acceso mas restrictivo.
    // protected void f() {}
    // No compila, tipo de retorno incompatible.
    // public int f() {}
    // No compila, el metodo f() en la clase Base no lanza ninguna excepcion.
    // public void f() throws Exception {}
    // Compila, pero esto no es una sobrescritura sino una sobrecarga.
    // @Override
    protected int f(int k) throws Exception {
        return 0;
    }

    @Override
    public void f() {
        System.out.println("En Subclase");
    }

    /*
    * Compila, ArrayList es una subclase de List e IllegalAccessException es una subclase de Exception. 
    Tambien compilaria si omitieramos 'throws IllegalAccessException'
    * Tambien compilaria con 'throws IllegalAccessException, IllegalArgumentException'
     */
    @Override
    public ArrayList<String> g() throws IllegalAccessException {
        return null;
    }

    // No compila, porque Exception no es una subclase de IllegalAccessException
    // public void h() throws Exception {}
    // Compila, porque este metodo no esta relacionado con el metodo i() de la superclase ya que los miembros privados no se heredan.
    public int i(int k) throws Exception {
        return 0;
    }

}

public class Leccion2 {

    public static void main(String[] args) {
        Subclase.s();
        /*
	* Esto es un ejemplo de polimorfismo, que veremos mas adelante, El objeto base es de tipo Base 
        pero hace referencia a una instancia de
	* Subclase. A pesar de eso, cuando accedemos a la variable var definida en ambas clases se muestra el valor 
        definido en la clase Base.
         */
        Base base = new Subclase();
        Subclase subclase = new Subclase();
        // 1
        System.out.println(base.var);
        // 2
        System.out.println(subclase.var);
        /*
	* En cambio, cuando accedemos al metodo f(), en ambos casos accedemos al metodo f de la clase Subclase.
         */
        base.f();
        subclase.f();
    }

}
