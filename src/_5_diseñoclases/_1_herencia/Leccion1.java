package _5_diseñoclases._1_herencia;

/**
 *
 * La herencia
 *
 * Cuando creamos una clase en Java, implÃ­citamente estamos heredando de la
 * clase java.lang.Object. Eso quiere decir que sus miembros publicos y
 * protected como los metodos toString, equals, etc., estan presentes en todas
 * las clases.
 *
 * De la misma manera, nuestras clases pueden heredar de exactamente de una sola
 * clase siempre que no sea final.
 *
 * La palabra clave extends es la que crea una relacion de herencia entre
 * clases. Una relacion que se conoce como ES-UN (IS-A).
 *
 * Es facil de ver que si enterprise es una instancia de PortaAviones,
 * PortaAviones es una subclase de BarceoDeGuerra y BarcoDeGuerra es una
 * subclase de Barco, entonces enterprise es una Barco.
 *
 * El metodo super() se utiliza para invocar al constructor de la superclase.
 *
 * Igual que hay un constructor por defecto, todo contructor tiene una llamada
 * super() implicita añadida por el compilador que podemos modificar pasando una
 * lista de argumentos si conviene.
 *
 * En resumen, cuando hay herencia los constructores deben cumplir unas reglas:
 *
 * 1. El meodo this() se utiliza para invocar otros constructores dentro de la
 * misma clase, y el metodo super() para invocar otros constructores de la clase
 * padre. Uno u otro debe ser la primera orden de un constructor.
 *
 * 2. Java añade una invocacion a super() implicitamente en cada constructor que
 * llama al costructor por defecto de la clase padre.
 *
 * 3. De no existir el constructor por defecto en la clase padre, el compilador
 * genera un error: 'Implicit super constructor is undefined'.
 *
 * 4. La solucion es doble, o bien creamos un constructor por defecto en la
 * clase padre, o bien modificamos la invocacion del metodo super() pasando los
 * argumentos adecuados.
 *
 * 5. La invocacion de super() debe ser la primera linea de codigo de un
 * costructor.
 *
 * 6. Como consecuencia del punto anterior, el constructor de la clase padre se
 * ejecuta antes que el constructor de una clase dada.
 *
 *
 * La palabra reservada this y el el metodo this() no estan relacionadas en
 * Java, this se utiliza para acceder a miembros (propiedades y metodos)
 * visibles de la clase (posiblemente heredados) y this() se utiliza desde un
 * constructor para invocar otro constructor de la misma clase.
 *
 * Con la palabra reservada super y super() pasa algo parecido, la palabra
 * reservada super se utiliza para acceder a miembros solo de la superclase, y
 * super() se utiliza para invocar a un constructor de la clase padre.
 */

/*
 * extends java.lang.Object implicito (Toda clase hereda de Object), un barco es un Object
 */
class Barco {

    protected double eslora;
    protected double manga;
    protected double calado;

    public Barco(double eslora, double manga, double calado) {
        this.eslora = eslora;
        this.manga = manga;
        this.calado = calado;

    }

    @Override
    public String toString() {
        /*
	* Aqui llamamos al metodo toString definido en java.lang.Object
         */
        return super.toString()
                + String.format("%nBarco [eslora=%s, manga=%s, calado=%s]%n",
                        eslora, manga, calado);

    }

}

class BarcoDeGuerra extends Barco {

    protected int marineros;

    // public BarcoDeGuerra() {} // No compila, porque la superclase no tiene un constructor por defecto.
    public BarcoDeGuerra() {
        super(0.0, 0.0, 0.0);
    }

    public BarcoDeGuerra(double eslora, double manga, double calado) {
        this(eslora, manga, calado, 0);
    }

    public BarcoDeGuerra(double eslora, double manga, double calado,
            int marineros) {
        super(eslora, manga, calado);
        this.marineros = marineros;
    }

    @Override
    public String toString() {
        return super.toString()
                + String.format("BarcoDeGuerra [marineros=%s]%n", marineros);
    }

}

final class PortaAviones extends BarcoDeGuerra {

    private int aviones;

    public PortaAviones(double eslora, double manga, double calado,
            int marineros, int aviones) {
        super(eslora, manga, calado, marineros);
        this.aviones = aviones;
    }

    @Override
    public String toString() {
        return super.toString()
                + String.format("PortaAviones [aviones=%s]", aviones);
    }

}

public class Leccion1 {

}
