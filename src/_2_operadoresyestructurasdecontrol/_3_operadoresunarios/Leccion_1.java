/**
 *
 * Los Operadores Unarios
 */
package _2_operadoresyestructurasdecontrol._3_operadoresunarios;

/*
1. Por definición un operador que requiere exactamente un operando o variable para funcionar.
2. Algunos como el incremento ++ o decremento -- pueden aplicarse en pre orden (a la izquierda, primero incrementa o decrementa la variable y después evalúa)
   o en post orden (a la derecha, primero se evalúa la expresión y despues se incrementa o decrementa la variable) teniendo un resultado ligeramente distinto en cada orden.
3. Los operadores mas +, menos - y admiración ! (negación lógica) también son operadores unarios.
 */

 /*
Operadores Unarios:
(+) Indica que un numero es positivo,  se supones que todos los numeros son positivos en java a menos que tenga el operador unarioa negarivo "-"
(-) Indica que un numero literal es negativo o una expresion
(++) Incrementa un valor en 1
(--) Decrementa un valor en 1
(!) Invierte un valor booleano
 */
public class Leccion_1 {

    public static void main(String[] args) {
        //Operador de negación (!)
        //invierte el valor de una expresion boleana
        {
            boolean bool = !true;
            System.out.println("bool: " + bool); //false
            bool = !bool;
            System.out.println("bool: " + bool); //true

            // int x=!5; no compila
            //boolean z=!0; no compila
            //boolean booleanFalse=0; no compila
            //boolean booleanTrue=1; no compila
        }

        //Complementos logicos (-)
        //invierte el signo de una expresion numerica
        {
            double decimalValue = 1.21;
            System.out.println(decimalValue); //1.21
            decimalValue = -decimalValue;
            System.out.println(decimalValue); //-1.21
            decimalValue = -decimalValue;
            System.out.println(decimalValue); //1.21
            //boolean y=-true; no compila
        }
        //Operador de Incremento y Decremento
        /*
        * Se aplican sobre operandos numericos
        * Tienen la prioridad mas alta, con respecto a los operadores binarios, son los primeros en aplicarse en una expresion
        */
        {
            //si el operador esta a la izquierda del operando, el nuevo valor se aplica para la misma expresion
            //si el operador esta a la derecha del operando, el nuevo valor se aplica en la siguiente expresion
            int contador = 0;
            System.out.println(contador); //0
            System.out.println(++contador); //1
            System.out.println(contador); //1
            System.out.println(contador--); //1
            System.out.println(contador); //0

            int x = 3;
            int y = ++x * 5 / x-- + --x; 
            //1) 4 * 5 / 4 + --x el primer x toma el valor de 4
            //2) 4 * 5 / 4 + --x //el segundo x toma el valor de 3 para para la expresion actual sigue teniendo el valor de 4
            //3) 4 * 5 / 4 + 2 //el tercer x tiene el valor de 3 del anterior decremento y vuelve a disminiur en 1
            //4) 20 / 4 + 2 ==> 5 + 2 => 7
            System.out.println("x es " + x);
            System.out.println("y es " + y);
        }
        // Durante el examen debes de ser capaz de evaluar expresiones como estas
        {
            int i = 1;
            int j = +2 + -3 * -++i;
            System.out.println("i: " + i + ", j:" + j); //i:2, j:8
            int a = 1, b = 2;
            int c = a++ * --b + 1;
            System.out.println("a: " + a + ", b:" + b + ", c:" + c); //a:2, b:1, c:2
        }

    }

}
