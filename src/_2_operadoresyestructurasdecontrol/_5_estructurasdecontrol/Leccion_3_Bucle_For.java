package _2_operadoresyestructurasdecontrol._5_estructurasdecontrol;

/**
 *
 * Las estructuras de control de Java
 *
 * Desde la versión 5.0 de Java hay dos tipos de bucles for. El for básico y el
 * for mejorado también llamado for-each.
 *
 */
public class Leccion_3_Bucle_For {

    public static void main(String[] args) {
        /*
        * Un bucle for tiene tres partes todas opcionales. De izquierda a derecha, la
        * primera es la de inicialización, la segunda es la condición de final de bucle
        * y la tercera la actualización.
         */

        //El for básico tiene este aspecto. Escribe de 0 a 9.
        for (int i = 0; i < 10; i++) {
            System.out.println(i);
        }

        //Este bucle es totalmente equivalente al anterior. Escribe de 0 a 9.
        {
            int i = 0;
            for (; i < 10;) {
                System.out.println(i);
                i++;
            }
        }
        System.out.println("1. Bucle Infinito");
        for (;;) {
            break; //si la quitamos el break, me saldria error de compilacion, ya que nunca entraria al resto de expresiones
        }

        System.out.println("2. Agregando Multiples Condiciones");
        {
            int x = 0;
            for (long y = 0, z = 4; x < 5 && y < 10; x++, y++) {
                System.out.println(y + " ");
            }
        }
        //result:   0 1   2   3   4
        /**
         * Es codigo desmuestra 3 variaciones para el bucle for ue probablemente
         * no has visto. 1) puedes declarar una variable tal como "x" antes que
         * la repeticion empiece y usarla despues se complete. 2) el bloque de
         * inicializacion, el bloque de expresion booleana y el bloque de
         * actualizacion, pueden incluir variables extras (TODAS DE CUALQUIER
         * TIPO NUMERICO) que pueden no referirse entre si. Por ejemplo "z" es
         * definid aen el bloque de inicializacion y nunca es usaa. Finalmente
         * el bloque de actualizacion puede modificar mulples variables.
         *
         *
         */

        System.out.println("3. Redeclarar una variable in el bloque de inicializacion");
        {
            int x = 0;
            //  for (long y = 0, x = 4; x < 5 && y < 10; x++, y++) {  } //no compila
            /**
             * Este ejemplo es similar al anterior, pero este no compila por el
             * bloque de inicializacion. la variable "x" del tipo long del
             * bloque de inicializacion ya fue declarada previamente antes de
             * iniciar el bucle for, como una variable tipo int
             */
            //podemos solucionarlo de la siguiente manera
            long y = 0;
            for (y = 0, x = 4; x < 5 && y < 10; x++, y++) {
                System.out.println("" + x);
            }
        }
        System.out.println("*4. Usando tipo de datos incompatibles en el bloque de inicializacion");
        {
            //for (long y = 0,int  x = 4; x < 5 && y < 10; x++, y++) { }
            //Este ejemplo es similar a los anteriores, pero con la diferencia de que esta vez no compilara
            //porque se intentan declarar dos tipos de datos diferentes en el bloque de inicializacion y esto 
            //no esta permitido.
        }

    }
}
