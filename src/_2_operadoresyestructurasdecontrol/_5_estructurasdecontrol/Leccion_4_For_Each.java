/**
 *
 * Las estructuras de control de Java
 *
 * Desde la versión 5.0 de Java hay dos tipos de bucles for. El for básico y el
 * for mejorado también llamado for-each.
 *
 */
package _2_operadoresyestructurasdecontrol._5_estructurasdecontrol;

import java.util.Arrays;
import java.util.List;

public class Leccion_4_For_Each {

    public static void main(String[] args) {
        /*
        * El bucle for mejorado, o bucle for-each introducido en Java 5.0 se
        * compone de dos partes una declaración de variable y un objeto sobre el que
        * iterar.
        * 
        * El objeto sobre el que iteramos debe ser un array o un objeto que
        * implementa java.lang.Iterable, lo que incluye la mayor parte de las
        * colecciones, que contenga elementos del mismo tipo que la variable declarada en el bucle.
         */
        System.out.println("1. Arreglo de String");
        String[] v = new String[]{"Oracle", "Certified", "Associate"};
        for (String s : v) {
            System.out.println(s);
        }

        System.out.println("2. Lista de String");
        List<String> listString = Arrays.asList(v);
        for (String s : listString) {
            System.out.println(s);
        }

        String miCadena = "Es una Cadena";
        //Error de compilacion por que no es un array, o no implementa java.lang.Iterable
        /*
        for (String s : miCadena) {
            System.out.println(s);
        }
         */
        System.out.println("3. Lista de String null");
        String[] arregloDeNulls = new String[3];
        for (String s : arregloDeNulls) {
            System.out.println(s);
        }

        /*
        * No compila por que s no es una instancia de string
        for (int s : arregloDeNulls) {
            System.out.println(s);
        }
         */
    }

}
