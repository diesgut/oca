/**
 * Promocion Numerica
 *
 * Promocion de numeros primitivos
 */
package _2_operadoresyestructurasdecontrol._2_operadoresaritmeticosbinarios;

/*
Recuerda las reglas de promoción numéricas:
1. El de mayor tamaño gana. (int < long < float < double)
2. Coma flotante gana sobre entero.
3. byte, short y char SIEMPRE se convierten a int automáticamente siempre que sean usados con operadores
 aritmeticos, y incluso si ninguno de los operandos es int 
 (exeptuando los operadores unarios ++ --, aplicando ++ en un short sigue siendo short).
4. El resultado de la expresión será del tipo que haya ganado.

 */
public class Leccion_3_Promocion_Numerica {

    /**
     * Primera Regla
     */
    int intVal1 = 1;
    long longVal33 = 33;
    // int result1 = intVal1 * longVal33; compilation error
    long result1 = intVal1 * longVal33;
    /**
     * Tercera Regla
     */
    //shortVal1 y shortVal2 se promocionan a int ante de la operacion
    short shortVal1 = 10;
    short shortVal2 = 3;
    int result2 = shortVal1 / shortVal2;
    /**
     * Todas las Reglas
     */
    //1) se promociona shortVal14 a int, solo por que es short, y viene siendo usado con un operador airtmerico binario
    //2) se promocinoa shortVal14 a float para que pueda ser divido por floatVal13
    //3) el reulstado del producto de shortVal14 y floatVal13, se promociona a double para que pueda ser dividido por doubleVal30
    short shortVal14 = 14;
    float floatVal13 = 13;
    double doubleVal30 = 30;
    double result3 = shortVal14 * floatVal13 / doubleVal30;
}
