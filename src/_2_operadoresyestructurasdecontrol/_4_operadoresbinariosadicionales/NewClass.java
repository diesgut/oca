/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package _2_operadoresyestructurasdecontrol._4_operadoresbinariosadicionales;

/**
 *
 * @author Diego
 */
public class NewClass {

    Integer valorInteger10;
    Integer valorInteger20;

    public NewClass() {
        this.valorInteger10=10;
        this.valorInteger20=20;
    }
    
    

    public Integer getValorInteger10() {
        return valorInteger10;
    }

    public void setValorInteger10(Integer valorInteger10) {
        this.valorInteger10 = valorInteger10;
    }

    public Integer getValorInteger20() {
        return valorInteger20;
    }

    public void setValorInteger20(Integer valorInteger20) {
        this.valorInteger20 = valorInteger20;
    }
    
    
}
