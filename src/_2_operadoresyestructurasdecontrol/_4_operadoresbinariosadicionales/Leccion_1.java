/**
 *
 * Operadores Binarios Adicionales
 */
package _2_operadoresyestructurasdecontrol._4_operadoresbinariosadicionales;

import java.io.File;

/*
Se encuentran los operadores de asignación simples y compuestos, operadores relacionales, operadores lógicos y operadores de igualdad. 
 */
public class Leccion_1 {

    public static void main(String[] args) {

        System.out.println("1. Operador de Asignación");
        {
            int x = 1;
            int a = 2, b = 3;

            //java promociona autamaticamente desde un tipo pequeño a un tipo de dato mas grande, pero lanza un 
            //error de compilación si se quiere guardar en un tipo pequeño un valor de tipo de dato mas grande
            // int x=1.0; //no compila, no se puede asignar un double a un int
            // short y=1921222; //no compila por que exede el rango del tipo short
            // int z=9F; //no compila
            // long t=192301398193810323;// no compila por que el rango de este valor pertenece a un tipo Integer
            // supera el valor de int, necesita el postfijo L, para guardarse en un tipo de dato long            
        }

        System.out.println("2. Casting de Valores Primitivos");
        {
            int x = (int) 1.0;
            short y = (short) 1921222;//Se guarda como 20678
            int z = (int) 9F;
            long t = 192301398193810323L;

            short a = 10;
            short b = 3;
            short c = (short) (a * y);// casteamos a short de lo contrario provocaria error de compilación, 
            //ya que el tipo short se promociona a int cuando se la aplica cualquier operador aritmetico
        }

        System.out.println("3. Operador de Asignación Compuesto");
        {
            //Los operadores de asignación compuesto que seran tomados en el examen seran: +=, -=
            //solo se pueden apllicar para variables que ya estan definidas, no se puede usar para declarar nuevas variables
            int x = 2, z = 3;
            x = x * z; //operador de asignación simple
            x *= z; //operador de asignación compuesto
            //
            long a = 10;
            int b = 5;
            //b = b * a; no compila por que el resultado se promocionada a long y b es del tipo int
            b *= x; //el operador compuesta ejecuta el cast a int, luego de operar las variables como long

            //otra cosa que hay que saber, es que se puede asignar otra expresión de asignación
            long d = 5;
            long e = (d = 3); //primero se establecr el valor de la variable d en 3 y luego la variable d se asigna a e
            System.out.println("d: " + d);
            System.out.println("e:" + e);

        }

        System.out.println("4. Operadores Relacionales");
        //Operadores Relacionales
        //Los 4 primeros opradores relaciones <,<=, >, >=, pueden ser aplciados a valores numericos primitivos unicamente, y devuelven un valor booleano
        //si los dos operandos numericos no son del mismo tipo, el tipo mas pequeño es promocionado al del mayor
        {
            int x = 10, y = 20, z = 10;
            Integer valorIneteger1 = new Integer(1);
            Integer valorIneteger2 = new Integer(2);
            System.out.println("x < y: " + (x < y));
            System.out.println("x <= y: " + (x <= y));
            System.out.println("x >= z: " + (x >= z));
            System.out.println("x > z: " + (x > z));
            System.out.println("valorIneteger1 > valorIneteger2 " + (valorIneteger1 < valorIneteger2));
            NewClass newClass = new NewClass();
            System.out.println("valorIneteger1 > valorIneteger2 " + (newClass.getValorInteger10() < valorIneteger2));
        }
        //el 5to operador relacional es el Operador relacional instanceof
        //a instanceof b, devuelve true si la referencia de b, es una instancia de una clase, subclass, o una clase que implemente a 

        System.out.println("5. Operadores Logicos");
        //Los operadores logicos (& AND, | Inclusive OR , ^ Exclusive Or), pueden ser aplicados a operadores numericos y booleanos
        //Cuando estos operadores son aplicados a datos booleanos, se les conoce como operadores logicos
        //Cuando estos operadores son aplicados a datos numericos, se les conoce como operadores bit a bit, ya que hacen comparacion de bit a bit
        //Los operadores logicos son:
        //AND & es solo verdadero si ambos operandos son verdadero
        //Inclusive  OR | es solo falso si ambos operadores son falsos
        //Exclusive  OR ^ es solo verdadero si los operandos son diferentes
        /*
        *Operadores de corto circuito
        finalmente, presentamos los operadores condicionales, && y ||, que a menudo se denominan operadores de cortocircuito. 
        Los operadores de cortocircuito son casi idénticos a los operadores lógicos, & y |, respectivamente, 
        excepto que el lado derecho de la expresión nunca se puede evaluar si el resultado final puede determinarse por el lado izquierdo de la expresión.
        Ejemplo:
         */
        {
            //   boolean x = true || (y < 4)
        }
        //La diferencia entre los operadores logicos y los de cortocircuito, es que los logicos evaluan ambos operandos,
        //mientras que los corto circuito evaluan de izquierda a derecha y se detiene si solo basta con la primera expresion
        //para tener un resultado
        /*
        if(x!=null && x.getValue()<5){ //en este caso si x es null, el operador condicional <5 nunca llega a ocurrir
            //hacer algo
        }
        
        if(x!=null & x.getValue()<5){ //como el operador logico evalua ambas expresiones, si x es null se cae cuando evalua el <5
            //hacer algo
        }
         */
        {
            int x = 6;
            boolean y = (x >= 6) || (++x <= 7);

            System.out.println("x: " + x + " y: " + y); //x: 6 y: true
            //notar que en este caso el valor de x no cambio
        }
        System.out.println("6. Operadores de Igualdad");
        //Evalua dos operandos y devuelve true si son iguales
        //1. Al comprarar dos valor numericos primitivos, si un valor numerico es de un diferente tipo, el valor se promociona
        //ejemplo: 5==5.00 devuelve true ya que el 5 se promueve a double
        //2. Compara dos valores booleanos
        //3. Compara dos objetos, incluyendo null y valores String
        {
            // boolean x = true == 3; //no compila
            //boolean y = false != "Giraffe"; //no compila
            //boolean z = 3 == "kangaroo"; //does not compile

            boolean a = false;
            boolean b = a = true;
            boolean c = (a = false);
            System.out.println("a: " + a + " b: " + b);//a: true b: true

            //Para la comparación de objetos la igualdad es aplicada a nivel de referencia mas no al objeto que apunta
            // En el examen puedes encontrarte con preguntas como esta, sobre clases quizas no te sean familiares, 
            //pero en general, la lógica es la misma para cualquier tipo de objeto.
            // El operador == evalúa si dos objetos apuntan al mismo objeto (Instancia) (misma dirección de memoria)
            File f1 = new File("myFile.txt");
            File f2 = new File("myFile.txt");
            File f3 = f1;
            System.out.println("f1 == f2" + (f1 == f2)); // false
            System.out.println("f3 == f1" + (f3 == f1)); // true
        }

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        System.out.println("#### udemy");
        // Al hacer un casting, o conversión de tipo, le estamos diciendo la compilador que ignore su comportamiento predeterminado.
        short x = 2, y = 3;
        short z = (short) (x * y);

        // Operadores de asignación compuestos. La expresión a = a * b; puede escribirse como asignación compuesta aunque no es exactamente igual.
        int a = 2, b = 3;
        a *= b;

        long m = 10;
        int n = 5;
        // n = m * n; // NO COMPILA
        // Cast implícito E1 op= E2 es lo mismo que E1 = (T) ((E1) op (E2)) donde T es de tipo E1. Puede haber pérdida de precisión
        n *= m;
        System.out.println("n: " + n); //n: 50

        // El resultado de una asignación es una expresión en sí mismo, por lo tanto, esto es totalmente válido.
        int p = 1;
        int q = (p = 2);
        System.out.println("p: " + p + ", q: " + q); //p: 2, q: 2

        // Los operadores lógicos de circuito corto &&, || son casi idénticos a los operadores logicos & y |. 
        // Sólo se diferencia en que tan pronto como el resultado es evidente, la evaluación se detiene.
        // La expresión entre paréntesis nunca llegará a evaluarse, es decir, no se lanzará una excepción por división entre cero.
        boolean bool4 = true || (p * 3 + p - 1 > p / 0);
        System.out.println("bool4: " + bool4); //bool4: true

        // Los operadores lógicos de circuito corto son muy útiles en casos como este, cuando queremos asegurarnos de que una instancia no es nula antes de invocar un método.
        // Por lo demás, los operadores relacionales, menor que, mayor que, menor o igual que, mayor o igual que son iguales al resto de lenguajes de programación.
        // El operador de igualdad y desigualdad en cambio, son como ves aqui. 
        // A menudo los programadores de Java noveles confunde el operador de asignación con el operador de comparación.
        String str = null;
        if (str != null && str.length() > 0) {//no entra aqui
            System.out.println("String no es nulo y tiene un valor " + str);
        }

    }

}
