/**
 *
 * Los Operadores
 */
package _2_operadoresyestructurasdecontrol._1_operadores;

/*
Los operadores son símbolos especiales que puede ser aplicados a variables, valores o literales.
Los operadores pueden ser unarios, binarios y ternarios que pueden aplicarse a uno, dos o tres operandos respectivamente.
La prioridad de los operadores Java es esta que  ves aqui (ordenados de más a menos prioritarios):

1. Post Operadores unarios expresion++, expresion-- incremento y decremento
2. Pre Operadores unarios ++expresion, ++expresion incremento y decremento
3. Operadores unarios + - !  mas, menos y negación
4. Operadores producto, división y módulo * / %  producto, división y módulo
5. Operadores suma y resta + -
6. Operadores de desplazamiento << >> >>> (Los operadores de desplazamiento a la izquierda, derecha y derecha con extensión de signo no forman parte del examen).
7. Operadores relacionales < > <= >= instanceof
8. Operadores de igualdad y desigualdad == !=
9. Operadores lógicos & ^ |
10. Operadores lógicos de circuito corto && ||
11. Operador ternario <expr booleana> ? <expre1> : <expre2>
12. Operadores de asignación = += -= *= /= %= &= ^= != <<= >>= >>>=


 */
public class Leccion_1 {

    public static void main(String[] args) {
        int i = 1;
        int j = -i;
        System.out.println("i: " + i + ", j:" + j); //i: 1, j:-1

        int y = 4;
        double x = 3 + 2 * --y; //el operador pre unario se ejecuta y el resultado lo muestra en la misma expresion 
        System.out.println("x: " + x + ", y: " + y); //primero se ejecuta el decremento de y, el resultado se multiplica por 2, finalmente se suma 3 y se proomociona de 9 a 9.0
        //x: 9.0, y: 3

        y = 4;
        x = 3 + 2 * y--; //el operador post unario se ejecuta y el resultado lo muestra en la siguiente expresion 
        System.out.println("x: " + x + ", y: " + y); //primero se ejecuta el decremento de y pero el resultado no lo muestra en la expresion, el resultado se multiplica por 2, finalmente se suma 3 y se proomociona de 9 a 9.0
        //x: 11.0, y: 3
    }

}
