package _2_operadoresyestructurasdecontrol._6_controlflujoavanzado;

/**
 *
 * El control de flujo avanzado
 *
 * El examen cuestionará tus conocimientos sobre la orden break y continue con
 * etiquetas en bucles anidados.
 *
 */
public class Leccion_1 {

    public static void main(String[] args) {

        System.out.println("1. Bucles Anidados");
        /*
        * Un bucle puede contener otros bucles (bucles anidados). En este ejemplo
        * intencionadamente he escrito un bucle clásico dentro de un bucle
        * for-each.
        * 
        * En Java una Matriz es una array de arrays. El bucle más externo recorre
        * en turno los arrays { 5, 2, 1, 3 }, { 3, 9, 8, 9 }, { 5, 7, 4, 7 }. Y el
        * bucle interno recorre cada elemento de cada array: 5, 2, 1, 3 después 3,
        * 9, 8, 9, etc.
         */
        int[][] myComplexArray = {{5, 2, 1, 3}, {3, 9, 8, 9},
        {5, 7, 12, 7}};
        for (int[] mySimpleArray : myComplexArray) {
            for (int i = 0; i < mySimpleArray.length; i++) {
                System.out.print(mySimpleArray[i] + "\t");
            }
            System.out.println();
        }

        System.out.println("2. Añadiendo Etiquetas Opcionales");
        /*
        * Los bloques 1) if, 2) switch y 3) bucles. Pueden tener etiquetas opcionales, un
        * identificador acabado con ':' (dos puntos), que se usa para saltar en un momento
        * determinado.
        * 
        * Tanto break como continue son una rotura de flujo, un salto, y
        * admiten una etiqueta opcional que si no la llevan se entiende que se
        * refire al bucle actual.
        * 
        * En un bucle for continue primero ejecuta el bloque de incremento de la variable y
        * después se verifica la condición de final de bucle.
        *
        * Las etiquetas opcionales siguen las mismas reglar que cualquier identificador
        * pero se recomiendan que estas esten en mayusculas y con un guion bajo para separar
        * palabras
         */
        System.out.println("2.1 La sentencia break");
        {
            /**
             * Como se vio en bucle siwth, un sentencia break tranfiere el flujo
             * de control fuera la sentencia encerrada. Lo mismo ocurre cuando
             * tenemos una sentencia break, en los bloques for
             *
             */
            OPTIONAL_LABEL:
            while (true) {
                break OPTIONAL_LABEL; //sin la etiqueta opcional, la sentencia break afectara al bucle mas cercano
            }
            //Las etiquetas opcionales nos permiten ejecutar un break, sobre un bucle externo superior
            int[][] list = {{1, 13, 5}, {1, 2, 5}, {2, 7, 2}};
            int searhValue = 2;
            int positionX = -1;
            int positionY = -1;
            PARENT_LOOP:
            for (int i = 0; i < list.length; i++) {
                for (int j = 0; j < list[i].length; j++) {
                    if (list[i][j] == searhValue) {
                        positionX = i;
                        positionY = j;
                        break PARENT_LOOP; //rompe el flujo hasta el bucle padre
                        //guardando en i y j, 1,1 respectivamente
                        //sin el break con la etiqueta PARENT_LOOP el resultado seria 2,0
                        //y sin ningun break el resultado seria 2,2
                    }
                }
            }

            if (positionX == -1 || positionY == -1) {
                System.out.println("Value " + searhValue + " not found ");
            } else {
                System.out.println(String.format("Value %s, found at (%s,%s)", searhValue, positionX, positionY));
            }
        }
        System.out.println("2.2 La sentencia continue");
        /**
         * Transfiere el flujo de control a la expresion booleana que determina si el flujo puede continuar.
         */
        FIRST_CHAR_LOOP: for (int intValue = 1; intValue <= 4; intValue++) {
            for (char charValue ='a'; charValue <= 'c'; charValue++) {
                if(intValue==2 || charValue=='b')
                    continue FIRST_CHAR_LOOP;
                System.out.println(" "+intValue+charValue);   //Al final Muestra 1a 3a 4a
            }
        }
      
        FIRST_CHAR_LOOP: for (int intValue = 1; intValue <= 4; intValue++) {
            for (char charValue ='a'; charValue <= 'c'; charValue++) {
                if(intValue==2 || charValue=='b')
                    continue;
                System.out.println(" "+intValue+charValue); //Al final Muestra  1a 1c 3a 3c 4a 4c
            }
        }
        
        

        // Ejemplo 1
        System.out.println("Continue + for");
        int[] v1 = {1, 3, 5, 6, 9};
        for (int i = 0; i < v1.length; i++) {
            if (v1[i] % 2 == 0) {
                continue;
            }
            System.out.println(v1[i]); //muetra impares
        }

        // Ejemplo 2
        etiqueta_01:
        for (int i = 0; i < 10; i++) {
            etiqueta_02:
            for (int j = 0; j < 10; j++) {
                // ignora si i par
                if (i % 2 == 0) {
                    /*
        * En un bucle for continue va al bloque de incremento y
        * después evalua la condición!!!
                     */
                    continue etiqueta_01;
                }
                // ignora si j <= i
                if (j <= i) {
                    continue;
                }
                System.out.println(i + ", " + j);
                // acaba cuando i * j > 9
                if (i * j > 9) {
                    break etiqueta_01;
                }
            }
        }

        // Ejemplo 3
        int a = 3, b = 4, c = 0;
        if_01:
        if (a != 0) {
            if (b != 0) {
                if (c == 0) {
                    break if_01;
                }
                System.out.println((a * b) / c);
            }
        }

        // Ejemplo 4
        int k = 0;
        while (k < 10) {
            System.out.println(k);
            if (k % 2 == 0) {
                /*
        * En un bucle do o while continue va a evaluar la condición de
        * la próxima iteración.
                 */
                ++k;
                continue;
            }
            k++;
        }
    }
}
