
public class Main {

    public static void main(String[] args) {
        System.out.println("01 Bloques de Construcción Java");
        System.out.println(" * 1 Estructura de una clase");
        System.out.println(" * 2 El metodo main()");
        System.out.println(" * 3 Declaración de paquetes e imports");
        System.out.println(" * 4 La Creación de Objetos");
        System.out.println(" * 5 Referencias a Objetos vs Tios Primitivos");
        System.out.println(" * 6 Declaración e inicialización de Variables");
        System.out.println(" * 7 Inicialización de Variables por Defecto");
        System.out.println(" * 8 Alcande de las Variables");
        System.out.println(" * 9 El Orden de los elementos en una clase");
        System.out.println(" * 10 La Destrucción de Objetos ");
        System.out.println(" * 11 Los Beneficios de Java ");
        System.out.println("02 Los Operadores y las Estructuras de Control");
        System.out.println(" * 1 Los Operadores");
        System.out.println(" * 2 Los operadores aritméticos binarios");
        System.out.println(" * 3 Los Operadores Unarios");
        System.out.println(" * 4 Operadores Binarios Adicionales");
        System.out.println(" * 5 Las Esructuras de Control Java");
        System.out.println(" * 6 El Control de Flujo Avanzado");
        System.out.println("03 Las API's del Núcleo de Java");
        System.out.println("04 Los Métodos y la Encapsulación");
        System.out.println("05 El Diseño de Clases");
        System.out.println("06 La Gestión de Excepciones");
    }

}
