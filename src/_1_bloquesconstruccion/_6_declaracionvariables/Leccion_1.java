/**
 *
 * Declaración e inicialización de Variables
 */
package _1_bloquesconstruccion._6_declaracionvariables;

/*
Una variable es el nombre de una porción de memoria que contiene datos, cuando declaramos una variable le damos un nombre,
o identificador válido, un tipo y opcionalmente un valor inicial.
 */
//Los identificadores (nombres de variables, metodos, clases, campos) siguen unas reglas de construccion muy precisas:
//1 Deben empezar con una letra, $ o _, Los siguientes caracteres pueden ser números.
//2 No se puede usar palabras clave de java como variables.
//3 Se recomienda no empezar un identificador con el carácter $ por que el compilador usa este símbolo internamente. 
public class Leccion_1 {

    public static void main(String[] args) {

        int i;
        i = 0;
        int j = 0, Ω = 0, œ, € = 0, ¥ = 0, ø = 0, _, $, ç;
        $ = €;
        // int void; // NO COMPILA, void es palabra clave

        //Se puede declarar e iniciar mas de una variable en la misma sentencia, pero deben ser del mismo tipo
        String s1, s2;
        String s3 = "tres", s4 = "cuatro";
        int i1, i2, i3 = 0;
       // String s1, String s2; //Error de compilacion,,, y el tipo debe ser declarado solo una vez
        
        
        int okidentificador;
        int $OKIDENTIFICADOR;
        int _okidentificador;
        int __$okidentificador;

        //no compilan
        //int 3didentificador;
        // int indigo@hotmail; @ no es letra
        //int diego.com; . no es letra
    }

}
