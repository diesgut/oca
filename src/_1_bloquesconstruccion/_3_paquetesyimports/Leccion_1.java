package _1_bloquesconstruccion._3_paquetesyimports;

/**
 *
 * Declaración de Paquetes e Imports
 */
/*
* Los paquetes son agrupaciones logicas
*Permiten organizar las clases mediante agrupaciones lógicas.
*package com.diego.pruebas
*import java.util.*; //se puede importar toda las clases con el comodin *
*import java.util.Date;
*En el caso de una clase repetida, se coloca el nombre completo con su paquete.
*No se necesita importar el paquete java.lang

 */
import java.util.Random;
import java.util.Date;
//import java.sql.Date; Error de compilación
import java.sql.*;

/**
 *
 * La declaracion de paquetes e import
 *
 * Cuando queremos utilizar una clase la tenemos que importar para que este
 * presente en el codigo. Algunas veces se dan confictos entre clases que
 * tienen el mismo nombre. En esos casos suele ser util escribir el nombre
 * completo de la clase, con su paquete. Recuerda que puedes importar todas las
 * clases de un paquete con el comodin (*).
 *
 * ATENCION: durante el examen de certificacion, las preguntas con lineas de
 * codigo numeradadas desde 1 representan programan enteros. Si no estan
 * numeradas a partir de 1 representan fragmentos y tenemos que suponer que el
 * codigo anterior es correcto y que se han hecho los imports correspondientes.
 */
public class Leccion_1 {

    public static void main(String[] args) {
        Random r = new Random();
        System.out.println(r.nextDouble());
        System.out.println(r.nextInt(10)); //un entero aleatorio entre 0 y 9
        // conflictos entre clases
        java.util.Date date1 = new java.util.Date();
        java.sql.Date date2 = new java.sql.Date(System.currentTimeMillis());
        Date date3 = new Date();
        System.out.println(date1);
        System.out.println(date2);
        System.out.println(date3); //date del paquete util
    }

}
