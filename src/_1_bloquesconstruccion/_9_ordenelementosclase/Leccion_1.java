/**
 *
 * El Orden de los elementos en una clase
 */
package _1_bloquesconstruccion._9_ordenelementosclase;

//1. Los comentarios pueden ir en cualquier parte.
//2. La declaración de package es opcional y ocupa el primer lugar.
//3. Los imports son opcionales y van después de la declaración package.
//4. La declaración de clase es obligatoria y va a continuación de los imports. En Java puede haber clases dentro de clases pero NO forma parte del examen OCA.
//5. La declaración de campos es opcional y pueden ir en cualquier parte de la clase (fuera de cualquier método).
//6. La declaración de métodos es opcional y puede ir en cualquier parte de la clase. En Java NO puede haber métodos dentro de métodos.
public class Leccion_1 {

    public static void main(String[] args) {

    }

}
