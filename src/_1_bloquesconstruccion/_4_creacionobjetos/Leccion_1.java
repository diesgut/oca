package _1_bloquesconstruccion._4_creacionobjetos;

/**
 *
 * La Creación de Objetos
 */
/*
*Todas las clases tienen un constructor por defecto el cual no ejecuta ninguna acción ni argumentos, 
pero podemos crear los contructores que creamos convenientes, pero al hacerlo este constructor por defecto 
dejara de existir teniendo la opcion de crearlo nuevamente de ser el caso.

*Pueden existir muchas clases por archivo, pero solo puede existir una clase pública y esta debe llevar 
el mismo nombre que el archivo donde se encuentra.


 */
public class Leccion_1 {

    //Los contructores se ejecutan luego de los bloque de inicialización de instancia.
    public Leccion_1(String argumentoString) {
        System.out.println("construcor con el argumento string " + argumentoString);

    }

    public Leccion_1(Integer argumentoNumerico) {
        System.out.println("construcor con el integer numerico " + argumentoNumerico);
    }

    // Bloques de inicialización de instancia. Se ejecutan antes de los contructores y en el orden el cual fueron escritos.
    {
        System.out.println("En bloque de instancia 1");
    }

    {
        System.out.println("En bloque instancia 2");
    }

    // Bloques de inicialización estaticos. Se ejecutan antes de los Bloques de inicialización de instancia y en el orden el cual fueron escritos.
    static {
        System.out.println("En bloque estatico 1");
    }

    static {
        System.out.println("En bloque estatico 2");
    }

    public static void main(String[] args) {
        /* ---------------------------------------------------------------------- */
        // error a menos que creemos un constructor sin argumentos
        // Leccion_1 construcorPorDefecto = new Leccion_1(); 
        /* ---------------------------------------------------------------------- */
        //Si corremos la clase sin crear un objeto, 
        //  Leccion_1 leccion_4 = new Leccion_1("mi argumento"); 
        // entonces solo mostraran los mensajes de los bloques estaticos
        /* ---------------------------------------------------------------------- */
        // Si corremos la clase creando un objeto, 
        Leccion_1 leccion_4 = new Leccion_1("mi argumento");
        //Entonces se mostraran, los mensajes de bloques estaticos, seguidos de los bloques de instancia
        //y los mensajes de los contructores, todo esto en el orden en que se encuentren en la clase
    }

}
