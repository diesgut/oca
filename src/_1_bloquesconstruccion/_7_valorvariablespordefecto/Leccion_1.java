/**
 *
 * Inicialización de Variables por Defecto
 */
package _1_bloquesconstruccion._7_valorvariablespordefecto;

/**
 *
 * La inicialización de variables por defecto
 *
 * Hay una diferencia sutil entre propiedades y variables locales. Las variables
 * locales son las variables que se declaran dentro de un método y deben ser
 * inicializadas antes de ser usadas.
 *
 * El resto de variables son los campos de la clase también se conocen como
 * "variables de instancia" porque cada instancia puede tener valores
 * diferentes. Si son estáticas, se comparten entre todas las instancias de esa
 * clase y se llaman "variables de clase" y vienen acompañadas de por la keyword
 * static en su declaracion.
 *
 * Las variables de instancia y de clase toman estos valores por defecto:
 *
 * boolean (false) byte, short, int y long (0) float y double (0.0), char
 * ('\u0000') vacio, todas las referencias a objetos (null).
 *
 */
import java.math.BigDecimal;

public class Leccion_1 {

    /**
     * Las variables que estan al nivel de la clase son denominadas variables de
     * instancia
     */
    String nombre; //null por defecto
    int edad; //0 por defecto
    BigDecimal monto; //null por defecto
    Integer promedio; //null por defecto
    boolean netEsMejor; //false por defecto
    char variableChar; // '\u0000' vacio

    /**
     * Las variables que estan al nivel de la clase que tienen la keyword static
     * en su declaracion son denominadas variables de clase
     */
    static int variableEstatica;//0 por defecto
    static short variableEstatica2; //0 por defecto
    static long variableEstatica3; //0 por defecto

    public Leccion_1() {
        System.out.println("variable instancia int edad " + edad);
        System.out.println("variable instancia BigDecimal monto " + monto);
        System.out.println("variable instancia boolean netEsMejor " + netEsMejor);
        System.out.println("variable instancia char variableChar " + variableChar);
    }

    //byte, short¸int, long por defecto 0
    //float, double por defecto 0.0
    //char por defecto '\u0000' (NULL)vacio
    //todas las variables por referencia inician con null
    public static void main(String[] args) {
        //Las variables que estan a nivel de metodos y otros bloques de codigo, son denomicadas variables locales

        //las variables locales deben ser inicializadas antes de usarse, de lo contrario generaran error de compilación
        int tamaño;
        Integer longitud;
        boolean javaEsMejor;
        // System.out.println(tamaño); //error de compilación

        String color;
        BigDecimal suma;
        Leccion_1 objeto = new Leccion_1();
        String cadenaTrue = "true";
        Boolean booleana = new Boolean(cadenaTrue); //si enviamos una cadena con el contenido true la variable sera true, otros contenidos seran false
        //System.out.println(color); //error de compilación
        //System.out.println(suma); //error de compilación
        System.out.println(booleana);

    }

    public void findAnswer(boolean check) {
        int answer;
        int onlyBranch; //Variable onlyBranch is not used
        if (check) {
            onlyBranch = 1;
            answer = 1;
        } else {
            answer = 2;
        }
        //El compilador de java es los suficientemente inteligente
        //para reconocer la inicializacion de variables en casos complejos
        System.out.println(answer);
        //System.out.println(onlyBranch); //error compilacion, puesto que cuando check sea false, la variable onlyBranch no estara iniciada
    }

}
