package _1_bloquesconstruccion._2_metodomain;

/**
 *
 * El Metodo Main
 */

/*
Un programa java empieza con el método main, es el punto de entrada e intermediario entre el inicio 
de un proceso java gestionado por la jvm y el comienzo del codigo de un programa.
 */
/**
 *
 * Para compilar una clase se usa las ordenes> 
 * > javac -d bin MiClase.java java //Compilamos la clase
 *
 * El comando javac es el compilador de Java 
 * La opción -d del compilador indica la ruta donde
 * guardar el resultado de la compilación, con extensión class.
 * 
 * Para ejecutar una clase compilada se usa las ordenes> 
 * > java -cp[<-classpath>] bin MiClase [<param1> [<param2>] [<"param 3">]...]
 * Donde el comando java es el interprete de bytecode para una arquitectura particular.
 * La opción -cp o -classpath del interprete indica dónde buscar las clases (tambien podrias
 * indicar jar's necesarios). A continuación del nombre de la clase podemos
 * pasar un número variable de parámetros.
 * 
 * 
 *
*windows
java -cp ".;C:\temp\otraUbicacion;c:\temp\myJar.jar" miPaquete.MiClase
*linux
java -cp ".:/tmp/otraUbicacion:/temp/myJar.jar" miPaquete.MiClase
java -cp "C:\temp\directorio\*" miPaquete.MiClase
* java com.foo.app.App
* 
* 
 */
/*
$ find . -name "*.class"
./target/classes/com/foo/app/App.class
$ CLASSPATH=target/classes/ java com.foo.app.App
Hello World!
$ java -cp target/classes com.foo.app.App
Hello World!
$ java -classpath .:/path/to/other-jars:target/classes com.foo.app.App
Hello World!
$ cd target/classes && java com.foo.app.App
Hello World!
*/
public class Leccion_1 {

    /**
     * @param args, guarda los parámetros que se pasan a main durante la
     * invocación. Como todo array en Java, se indexan a partir de cero.
     */
    public static void main(String[] args) {
        System.out.println("Hola mundo.");
        for (int i = 0; i < args.length; i++) {
            System.out.println(args[i]);
        }
    }

}
