package _1_bloquesconstruccion._2_metodomain;

//Crear Clases y Compilar Con Paquetes
public class Leccion_2 {
    /*
    1) Creamos una carpeta de nombre "temp" en el disco "c"
    2) Dentro de la carpeta temp creamos dos carpetas; "paquete1" y "paquete2"
    3) Creamos una clase "ClaseA.java" con su respectivo metodo main en la carpeta paquete1
    4) Creamos una clase "ClaseB.java" con su respectivo metodo main en la carpeta paquete2
    5) En una ventana de linea de comandos nos situamos en la carpeta temp creada previamente
    6) Ejecutamos el comando; javac paquete1/ClaseA.java paquete2/ClaseB.java para compilar las clases
    7) Ejecutamos el comando; java paquete1.ClaseA o java paquete2.ClaseB para ejecutar las clases compiladas
    */
}
