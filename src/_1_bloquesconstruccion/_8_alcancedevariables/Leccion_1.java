/**
 *
 * Alcande de las variables
 */
package _1_bloquesconstruccion._8_alcancedevariables;

//Cuando declaramos una variable en un método o en bloque en general, esa variable solo es visible en ese método o bloque. 
//Las variables locales, incluidos los parámetros de los métodos, tienen alcance local. 
//Es decir que solo son visible en el método que las contiene. 
//Las variables declaradas en un bloque, tienen alcance solo en ese bloque. 
public class Leccion_1 {

    public static void main(String[] args) {
        int i = 0;
        {
            int j = 0;
            {
                int k = 0;
                i++; // OK
                j++; // OK
                k++; // OK
            }
            i++; // OK
            j++; // OK
//			k++;
        }
        i++; // OK
//		j++;
//		k++;
    }

}
