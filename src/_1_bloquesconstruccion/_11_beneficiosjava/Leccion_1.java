/**
 *
 * Los Beneficios de Java
 * 
 */
package _1_bloquesconstruccion._11_beneficiosjava;

//En esta lección veremos cuáles son los beneficios o características de Java desde el punto de vista del examen oficial.
//Desde el punto de vista de este examen Java tiene las siguientes características:
// 1. Java es un lenguaje de programación orientado a objetos la cual es su principal forma de organización de código. 
// 2. Java soporta diferentes modificadores de acceso que protegen los datos de accesos accidentales o malintencionados, 
    //lo que se conoce como encapsulación, la cual es conocida como una característica particular de la programación orientada a objetos. 
// 3. Java es un lenguaje de programación interpretado lo que significa que cuando compilado,  generamos bytecode un código intermedio entre 
    //el código fuente y el código maquina de arquitectura en particular, por ese java es independiente de la plataforma. 
// 4. Java es robusto y previene la pérdida de memoria gracias a la gestión del garbage collector.
// 5. Simple. (Elimina los puntores y la aritmética de punteros de C++.)
// 6. Java es seguro porque ejecuta el código dentro de una máquina virtual (Java Virtual Machine), lo que crea un entorno de ejecución que aisle el código de posibles problemas. 

public class Leccion_1 {

    public static void main(String[] args) {

    }

}
