/**
 *
 * Referencias a Objetos
 */
package _1_bloquesconstruccion._5_referenciaobjetosvsprimitivos;

public class Leccion_2 {

    /*
    --Referencia de Objetos
    * Tienen metodos.
    * Pueden instanciarse con null.
    * Los objetos en sí no tienen nombre, solo una dirección de memoria y solo pueden ser accedidos mediante una referencia (variable).
    * Su valor por defecto es null
     */
    String valorPorDefecto;

    public static void main(String[] args) {
        /*
         * Hay varias formas de asignar un valor a una referencia: 
          1 Creando una instancia de la clase con new. 
          2 Asignando otra referencia del mismo
          3 Asignando el valor de retorno de un método.
         */
        String hola1 = new String("hola"), hola2 = hola1, hola3 = "hola", hola4 = hola2.toUpperCase();
        //hola1 y hola2 hacen referencia al mismo objeto
        /**
         * hola3 hace referencia al mismo objeto de hola1 y hola2 esto es debido
         * a que java separa los valores string para que apunten al mismo objeto
         * con igual mismo valor
         */
        //hola4 hace referencia a otro objeto debido a que el valor cambio
        System.out.println("Variable hola " + hola1.hashCode());    //3208380
        System.out.println("Variable hola2 " + hola2.hashCode());   //3208380
        System.out.println("Variable hola3 " + hola3.hashCode());   //3208380
        System.out.println("Variable hola4 " + hola4.hashCode());   //2223292

        Leccion_2 leccion = new Leccion_2();
        System.out.println("valorPorDefecto " + leccion.valorPorDefecto); //null
    }

}
