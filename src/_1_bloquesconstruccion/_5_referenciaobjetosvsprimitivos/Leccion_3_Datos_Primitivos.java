package _1_bloquesconstruccion._5_referenciaobjetosvsprimitivos;

/**
 * El examen asume que sabemos bien acerca de los ocho tipos de datos, sus
 * tamaños relativos y lo que se puede almacenar en ellos
 */
public class Leccion_3_Datos_Primitivos {

    /*
    byte    -> Entero con signo -> Valor por defecto 0, Tamano 8 bits, rango -128 a 127
    short   -> Entero con signo -> Valor por defecto 0, Tamano 16 bits, rango -32,768 a 32,767
    int     -> Entero con signo -> Valor por defecto 0, Tamano 32 bits, rango -2,147,483,648 a 2,147,483,647 //no lleva ,
    long    -> Entero con signo -> Valor por defecto 0, Tamano 64 bits, rango -2,147,483,648 a 2,147,483,647 //no lleva ,
    float   -> Punto flotante(Decimal) -> Valor por defecto 0.0, Tamano 32 bits, rango -2,147,483,648 a 2,147,483,647 //no lleva ,
    double  -> Punto flotante(Decimal) -> Valor por defecto 0.0, Tamano 64 bits, rango -2,147,483,648 a 2,147,483,647 //no lleva ,
    char    -> Caracter UNICODE -> Valor por defecto \u0000, Tamano 16 bits, rango \u0000 a \uFFFF
    boolean -> Verdadero o Falso -> Valor por defecto false, Tamano 1 bit usado en 32 bit integer
     */
 /*
    1) cada tipo numérico usa dos veces tantos bits como un tipo similar más pequeño. 
    por ejemplo, short usa el doble de bits que los byte.
     */
 /*
    no se le preguntará sobre los tamaños exactos de la mayoría de estos tipos. 
    debes saber que un byte puede contener un valor de -128 a 127. 
    para no quedarte atascado memorizando esto, veamos cómo Java obtiene eso. 
    un bye es de 8 bits. un bit tiene dos posibles valores.
    Estas son definiciones básicas de informática que deberías memorizar.
    2 elevado a la 8 es = 2x2=4x2=8x2=16x2=32x2=64x2=128x2=256. 
    como 0 debe incluirse en el rango, Java lo retira del lado positivo. 
    o si no te gustan las matemáticas, simplemente puedes memorizarlas.
    
   java usa la cantidad de bits cuando calcula la cantidad de memoria que se puede reservar 
    para su variable. por ejemplo, asigna java 32 bis si escribes esto:
     */
    public static void main(String[] arg) {
        int num;
        //una forma de conocer el valor maximo de integer es
        int maximo = Integer.MAX_VALUE;
        System.out.println("maximo: " + maximo);
        //otra forma de conocer el valor maximo de int es:
        //Como el tamaño de int son 32 bits podemos hacer la siguiente operacion
        // 2 elevado a 32, el resultado lo dividimos entre 2 
        //y para el lado negativo lo ponemos tal cual y para el psotivo le restamos 1
    }

}
