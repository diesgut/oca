/**
 *
 * Tipos Primitivos
 */
package _1_bloquesconstruccion._5_referenciaobjetosvsprimitivos;

public class Leccion_1 {

    int i2;

    /*
        --Primitivos
        * Los Tipos Primitivos no tienen metodos.
        * No pueden instanciarse con null.
        * Los tipos de datos primitivos son:
            byte    -> Valor Entero con signo   ->  Valor por defecto 0, Tamano 8 bits, rango -128 a 127
            short   -> Valor Entero con signo   ->  Valor por defecto 0, Tamano 16 bits, rango -32,768 a 32,767
            int     -> Valor Entero con signo   ->  Valor por defecto 0, Tamano 32 bits, rango -2,147,483,648 a 2,147,483,647 //no lleva ,
            long    -> Valor Entero con signo   ->  Valor por defecto 0, Tamano 64 bits,
            float   -> Valor con punto flotante ->  Valor por defecto 0.0, Tamano 32 bits
            double  -> Valor con punto flotante ->  Valor por defecto 0.0, Tamano 64 bits
            char    -> Caracter UNICODE         ->  Valor por defecto \u0000, Tamano 16 bits, rango \u0000 a \uFFFF
            boolean -> Verdadero o Falso        ->  Valor por defecto false, Tamano 1 bit usado en 32 bit integer
        *
     */
    static public void main(String[] args) {

        Leccion_1 leccion_1 = new Leccion_1();
        leccion_1.valorTipoChar();
        /*
        --Valor Literal Numerico
        * Cuando un numero aparece en el codigo se le llama literal
        * Los literales numéricos, pueden contener valores:
                                numéricos en base 10 (tambien es base 10= 1_123), 
                                octales         032         (comienzan con el prefijo 0), 
                                hexadecimales   0X1A        (comienzan con el prefijo 0X | 0x), 
                                binarios        0B101010    (comienzan con el prefijo 0B | 0b) 
                                y _ (guines) SIEMPRE QUE ESTEN ENTRE DOS NUMEROS.
         */
        //
        /*
        --Valores Enteros
        *Un valor literal (numérico) sin decimales por defecto es int.
         */
        leccion_1.valoresEnteros();
        /*Fin*/

        //
        /*
        --Valores con Punto Flotante
        *Un literal con decimales (punto flotante), por defecto es double, excepto si contiene el sufijo f o F. 
         */
        leccion_1.valoresConPuntoFlotante();
        /*Fin*/

        //
        /*
        --OPERANDO VALORES NUMERICOS
        *Cuando se opera con diferentes tipos numéricos el tipo del resultado es el tipo del
        operando mayor: int < long < float < double
         */
        leccion_1.operandoValoresNumericos();
        /*Fin*/
    }

    public void valorTipoChar() {
        /*
        --Char
        *Un tipo char no puede contener numeros negativos.
        *Puede contener codigo unicode
        *Puede contener enteros positivos
         */
        char a = '\u0000'; //vacio
        System.out.println("Char (a) " + a);
        //  char b = -1; ///un char no puede contener enteros negativos
        char c = 1;
        System.out.println("Char (c) " + c);
        char d = '\uFFFF';
        System.out.println("Char (d) " + d);
        //char c = 'A'; | char c = 65;
    }

    public void valoresEnteros() {
        /*-------------------------- byte --------------------------*/
        byte byteValorMinimo = Byte.MIN_VALUE; //valor minimo -128
        byte byteValorMaximo = --byteValorMinimo;
        System.out.println("byteValorMaximo " + byteValorMaximo); //valor maximo 127
        //En conclusion, solo puede contener valores entre -128 y 127 sin problemas
        /*-------------------------- fin --------------------------*/
        //
        /*-------------------------- short --------------------------*/
        short shortValorMaximo = Short.MAX_VALUE; //Valor Maximo 32_767
        short shortValorMinimo = ++shortValorMaximo;
        System.out.println("shortValorMinimo " + shortValorMinimo); //valor minimo -32_768
        //En conclusion, solo puede contener valores entre -32_768 y 32_767 sin problemas
        /*-------------------------- fin --------------------------*/
        //
        /*-------------------------- int --------------------------*/
        int intValorMaximo = Integer.MAX_VALUE; //Valor Maximo 2_147_483_647
        int intValorMinimo = ++intValorMaximo;
        //intValorMinimo=Integer.MIN_VALUE;
        System.out.println("intValorMinimo " + intValorMinimo); //-2_147_483_648
        //En conclusion, solo puede contener valores entre -2_147_483_648 y 2_147_483_647 sin problemas
        /*-------------------------- fin --------------------------*/
        //
        /*-------------------------- long --------------------------*/
        long longValorMaximo = Long.MAX_VALUE; //Valor Maximo //9_223_372_036_854_775_807L
        long longValorMinimo = ++longValorMaximo;
        System.out.println("longValorMinimo " + longValorMinimo); //-9_223_372_036_854_775_808L

        //Al tipo long puede asignarle cualquier valor entre el rango de valores int, sin problemas
        long longMaximoValorInt = Integer.MAX_VALUE;
        long longMaximoMinimoInt = Integer.MIN_VALUE;
        //Pero si el valor excede el rango de int, tener en cuenta lo siguiente
        // long longValorError = 2_147_483_648; //Error
        long longValorCorrecto = 2_147_483_648L; //Es valor si es correcto

        //Recordar que los literales numericos sin punto flotantes, son int por defecto.
        //la variable longValorError tiene el error de compilacion (integer number too large: 2147483648)
        //la variable longValorCorrecto especificamos que es long son el sufijo L en el valor, por lo tanto no habria problemas
        /*------------------------------------------------------------------------------*/
        //
        /*------------------------------------------------------------------------------*/
        /**
         * El dia del examen estaria bien que supieras los limites de los tipos
         * numericos, particularmente del tipo int, aunque no te los preguntaran
         * directamente. Cuul crees que es la salida de este codigo?
         *
         * Como ves si sumas uno a valor entero maximo obtienes el valor entero
         * minimo y paralelamente si restas uno al valor entero minimo obtienes
         * el valor entero maximo.
         *
         */
    }

    public void valoresConPuntoFlotante() {
        /*-------------------------- FLOAT --------------------------*/
        float floatValorMaximo = Float.MAX_VALUE; //Valor Maximo 3.4028235E38F
        float floatValorMinimo = ++floatValorMaximo;
        float floatValorMinimo2 = Float.MIN_VALUE;
        System.out.println("floatValorMinimo " + floatValorMinimo);
        System.out.println("floatValorMinimo2 " + floatValorMinimo2);

        float floatValorByte = 127; //Puede contener el rango de valores byte sin problemas
        float floatValorShort = 32_767; //Puede contener el rango de valores short sin problemas
        float floatValorInt = 2_147_483_647; //Puede contener el rango de valores int sin problemas
        float floatValorLong = 9_223_372_036_854_775_807F; //Puede contener el rango de valores long con el sufijo F
        // float floatDecimal1 = 1.0; Error puesto que los literales decimales por defecto son double //incompatible types: possible lossy conversion from double to float
        float floatDecimal2 = 1.0F; //Funciona sin problemas al colocar el sufijo F
        /*-------------------------- fin --------------------------*/
        //
        /*-------------------------- DOUBLE --------------------------*/
        double doubleValorMaximo = Double.MAX_VALUE; //1.7976931348623157E308
        double doubleValorMinimo = Double.MIN_VALUE; //4.9E-324
        double doubleValorByte = 127; //Puede contener el rango de valores byte sin problemas
        double doubleValorShort = 32_767; //Puede contener el rango de valores short sin problemas
        double doubleValorInt = 2_147_483_647; //Puede contener el rango de valores int sin problemas
        double doubleValorLong = 9_223_372_036_854_775_807F; //Puede contener el rango de valores long con el sufijo F
        double doubleValorLong2 = 9_223_372_036_854_775_807D; //Puede contener el rango de valores long con el sufijo D
        double doubleValor1 = 1.0;
        double doubleValor2 = 1_00;
        double doubleValor3 = 1_00.00;
        double doubleValor4 = 1_00.0_0;
        //double doubleValor5 = _1_00.0_0; error compilacion
        //double doubleValor6 = 1_00.00_; //error compilacion
        //double doubleValor7 = 1_00_.0_0; // error compilacion
        //double doubleValor9 = 1_00._0_0; //error compilacion
        /*-------------------------- fin --------------------------*/
    }

    public void operandoValoresNumericos() {
        float valorFloat = 1.0F;
        double doubleSumatoria = 1 + 1D;
        // float floatSumatoria = 1 + 1.0; //error de compilación 
        float floatSumatoria = 1 + 1.0F;
    }

}
