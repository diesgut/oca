package _1_bloquesconstruccion._1_estructuraclase; //nombre de paquete, si la clase esta en el paquete default no es necesario

/**
 *
 * Estructura de una Clase
 */

/*
Unidad basica de desarrollo, a partir de estas se crean objetos u instancias de la clase, los objetos representan 
el estado de nuestro programa, las clases constan de:

* métodos (procedimientos, funciones) 
* campos (propiedades, variables) 

conjuntamente se llaman miembros de una clase.
Las variables muestran el estado del programa y los métodos operan sobre esos estados. 

Ademas de las clases hay otras estructuras, como interfaces, enums.
 */
 /*
* Una clase esta definida por la palabra clave (keyword) "class"
* Puede contener un nombre de paquete.
* Puede contener uno o mas ordenes import.
* Un fichero con extension *.java puede contener uno o muchas clases.
* En un fichero java solo puede existir una clase publica y debe llevar el nombre del fichero.
 */
public class Leccion_1 {

    private String nombre; //Campo o propiedad de la clase

    /*
    * Método print
     */
    public void print() {
        System.out.println("Bienvenido a OCA");
    }

}

//java no requiere que una clase sea "public", de no colcar el scope (alcance de la variable)
//esta tendre el scope "default" implicito, pero no podemos colocarle el keyword default explicitamente 
class Animal {

}
//Tipos de comentarios

//Comentario de una sola linea

/* Comentario multiple
* 
 */

 /*
* //comentario valido
 */
// comentario valido
// // comentario valido sigue siendo de una linea
// /* comentario valido, sigue siendo de una linea */

/*
* En el caso de la siguiente linea, nos produciria error de compilación
* Si no tuviéramos el comentario de una sola se daria el error
* Debido a que el comentario multilinea no esta cerrado correctamente
 */
// /* /* */ */
