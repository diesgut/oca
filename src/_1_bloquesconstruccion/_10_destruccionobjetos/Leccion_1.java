/**
 *
 * La Destrucción de Objetos
 */
package _1_bloquesconstruccion._10_destruccionobjetos;

import java.util.ArrayList;
import java.util.List;

/*
-Todos los objetos de java se guardan en la MEMORIA HEAP, cuando nuestro programa se queda sin memoria, nos lanzara
 la excepcion "out of memory"
Debes tener en cuenta que no hay ninguna garantía de que el método System.gc() (Gargabe Collector) se ejecute cuando lo invocas.
Java se toma esta invocación como una mera sugerencia pero perfectamente puede ignorarla.
También debes saber cuándo el espacio que ocupa un objeto es candidato a ser liberado:
 */
// 1. Será candidato cuando un objeto no tiene ninguna referencia.
// 2. O cuando todas las referencias a un objeto están fuera de alcance.
// Nota: recordar que las referencias son variables que son usadas para acceder al contenido de objetos. una referencia
//puede ser asignada a otra referencia, pasada a un metodo o retornada en otro metodo.

/* Diferencia entre referencias y objeto */
/**
 * Referencias: son variables que tiene un nombre y son usadas para acceder al
 * contenido de objetos. una referencia puede ser asignada a otra referencia,
 * pasada a un metodo o retornada en otro metodo. Las referencias tienen el
 * mismo tamaño. (pueden o no pueden crearse en el heap)
 */
/**
 * Objetos: estan localizados en la memoria heap y no tienen nombre. No hay
 * forma de acceder a estos objetos excepto a traves de referencias. Su tamaño
 * es variable dependiendo de la definicion de la calse.
 */
public class Leccion_1 {

    public static void main(String[] args) {
        System.out.println("Llamando al Garbage Collector");
        Temporal t1 = new Temporal();
        System.out.println("t1 direccion en memoria " + t1.hashCode());
        {
            Temporal t2 = new Temporal();
            System.out.println("t2 direccion en memoria " + t2.hashCode());
            // t2 = null;
        }
        t1 = null;
        System.out.println("inicio de llamado");
        System.gc();
        /**
         * Como ves en este ejemplo, a la instancia t1 le asigno el valor null
         * justo antes de invocar al método System.gc() y además la instancia t2
         * está fuera de rango, sin embargo el Garbage Collector sólo llama una
         * vez al método finalize(), para recuperar el espacio que ocupaba el
         * objeto asignado a t1.
         */

        String one, two;
        one = new String("a"); //se crea un objeto a y se guarda una referencia en la variable one
        two = new String("b"); //se crea un objeto b y se guarda una referencia en la variable two
        one = two; //se guarda una referencia al objeto b en la variable one
        String tree = one; //se guarda una referencia al objeto b en la variable tree ahora no existe referencias al objeto a
        one = null;
        System.out.println("tres " + two);
    }
}

class Temporal {

    @Override
    protected void finalize() throws Throwable { //el metodo finalize solo se ejecuta cuando el objeto es elegible por el garbage collector
        super.finalize();
        System.out.println("En filanize() de " + this.hashCode());
    }
}
